Changelog
=========

Changes in next version
-----------------------

Here is a list of changes that are not yet part of a release, but that will
appear in next release.

### Features

* New button in radical selection window to clear all radicals at once.

Changes in 0.5.2
----------------

### Bug Fixes

* Dictionary download crashed on Android 12+.
* Dictionaries are now downloaded in app data instead of cache
* Dictionary update no longer marks the dictionary as not downloaded

Changes in 0.5.1
----------------

### Bug Fixes

* The Japanese full-width question mark is now understood as a question mark
  for regex search
* A bug caused a crash in some Android versions (at least Android 8). The
  reason why the bug is specific to Android versions is unknown, but a
  workaround was implemented.

Changes in 0.5.0
----------------

### Features

* KanjiVG support was added. This allows to show kanji stroke order.
* Search was extended to support '?' and '*' characters. '?' means a single
character, while '*' means any number of characters, including none.
* Tatoeba support was added. This allows you to search for examples and their
translations.

### UI

* Split About activity to a separate License activity and redesign it

### Translations

* Update French and Ukrainian translations.

Changes in 0.4.1
----------------

This is mostly a maintenance release, with some code changes to make it more
maintainable and resolve automatic warnings.

### Features

* New pitch accent display styles.  You can now see the pitch accent information
as before in a box next to the head word, as a pitch diagram below the head
word, or as a contour around the hiragana pronunciation.

Changes in 0.4.0
----------------

### Bug Fix

* Fix result ordering and ensure exact matches are shown first.
* Fix issues with the furigana view by switching to a different implementation.
  This fixes the issues where the furigana was not fully visible if it took
  too much space, as well as the view disappearing completely when the text was
  too large.
* Fixed furigana with katakana in headword. It used to make the furigana matching
  fail when the reading did not contain exactly these katakana (eg. if the reading
  had hiragana instead).

### Features

* It is now possible to filter the list of dictionaries by target language.
* New dictionary: kanjidic for kanji information. This comes with a new
  tab system in the main activity that lets you switch between word and kanji
  search results.

### UI

* Added support for dark theme.
* Moved search bar to the app bar, leaving more space for results.

### Translations

* Update French and Ukrainian translations.

Changes in 0.3.2.1
------------------

Previous version was not released properly, so here is a new version with
all the necessary changes, sorry for the inconvenience!

Changes In 0.3.2
----------------

### Features

* New preference for showing pronunciation, as furigana, kana or romaji.

### UI Fixes

* Language is shown in front of translations, as this was sometimes ambiguous
  when using dictionaries in different languages.
* Show more info about translations when the info exists.
* Move head word to top of entry, instead of left position where it could take
  too much space.

Changes In 0.3.1.1
------------------

### Bug Fix

* Fix a bug on Android 9+ where dictionaries were not downloading because of
  a missing permission (running a foreground service).

Changes In 0.3.1
----------------

### Features

* New dictionary from [Jibiki](https://jibiki.fr).

### UI Fixes

* Clicking the back button when the radical panel is open will now close the panel
  instead of exiting the app. No change the behavior when the panel is already
  closed.
* Report what was tried when there is no result.
* Update feedback text in case of error.
* Dictionary list was not redrawn after refreshing the list.

### Translations

* Update French translation.
* Add Simplified Chinese and Ukrainian translation.
* Translations are now hosted on [fedora's weblate](https://translate.fedoraproject.org/projects/nani/).

Changes In 0.3.0
----------------

### Bug Fixes

* Downloading dictionaries would make some devices almost freeze. Fixed by
  reducing the rate of UI and notification updates, and by ensuring the file
  is not kept in memory during its transfer.

### UI Fixes

* Dictionary download notification now opens the dictionary view.
* Dictionary download notification promoted to a foreground service, so it
  can queue downloads
* Dictionary view now updates live, even after re-entering it or changing
  orientation of the device.
* List of available dictionaries did not always update properly.

### Features

* Add a Help screen and three topics: kanji input by component, romaji input and
  pitch accent patterns.
* Add a button for direct help access on radical input.
* Application can now download dictionaries from Wadoku.
* Application can now show pitch accent with proper dictionary from Wadoku.

### Translations

* Update fr translation.

Changes In 0.2.3
----------------

### Bug Fixes

* Do not highlight dictionaries that have not been downloaded, even if there is
  a new version.
* Fix a crash on older devices that was caused by incorrect handling of dictionary
  icons.
* Fix a crash on notification on older devices, caused by the use of a vector
  icon in the notification, which was not supported.
* Fix default radical button size which was too small.
* Fix ordering of radicals by stroke count on some devices.

### UI Fixes

* Fix white on white text display on some older devices.
* Fix alignment in dictionary list.
* Small alignment and size fixes in dictionary download screen.
* Add back button to action bars to better navigate between screens.
* Use a star to notify dictionary updates instead of background color.

### Features

* When two identical results come from different dictionaries, they are now
  merged (eg. if you have two dictionaries for two different languages, you
  will see the meanings for both languages in the same entry).

### Translations

* Update fr translation

Changes In 0.2.2.1
------------------

### Features

* Display expected file size and entry count in dictionary list.

### Bug Fixes

* Scrolling in dictionary view was slow because we computed the hash of downloaded
  files.  We now download to a temporary file, so checking a download now is
  instantaneous and scrolling is smooth again.

Changes In 0.2.2
-------------------

### Features

* Dictionary list is now downloaded from the website, so new dictionaries will
  not always require an app update.
* Show a message when a file is corrupted and delete it.
* Show partial download status when a file failed partway.
* New pause button to stop downloads partway and retry later.
* Detect dictionary updates, display them with a yellow background in the list
  of dictionaries.
* Downloaded dictionaries are now on top of the list.

### Bug Fixes

* When a file is already downloaded, updating it will append to it instead
  of overriding it.
* When a file is partially downloaded but updated on server, remove the old
  partial file as continuing download would cause a hash mismatch.

### Translations

* Update fr translation

Changes In 0.2.1
-----------------

### Features

* New settings: radical size
* Download a control sum with dictionaries
* Support resuming partial downloads

### Bug Fixes

* Show dictionary sizes in appropriate unit, to prevent showing 0MB on
  small files
* Show a notification when downloading, which helps keeping the app alive
  while downloading.

### Translations

* Update fr translation

Changes In 0.2
-----------------

### Features

* Hints are shown when a dictionary is missing
* Support romaji and katakana input
* Add Kanji lookup by component/radical [#7](https://framagit.org/nani-project/nani-app/issues/7)

### Bug Fixes

* Preserve results when device orientation changes

### Misc

* Update description for Fdroid
* Asynchronous search for better user experience
* Update dictionary list icons color on download or remove

### Translations

* Update fr translation

