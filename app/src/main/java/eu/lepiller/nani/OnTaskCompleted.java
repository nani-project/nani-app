package eu.lepiller.nani;

public interface OnTaskCompleted<Result> {
    void onTaskCompleted(Result result);
}
