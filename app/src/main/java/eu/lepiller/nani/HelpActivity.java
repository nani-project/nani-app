package eu.lepiller.nani;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Button romaji_button = findViewById(R.id.help_topic_romaji);
        Button radical_button = findViewById(R.id.help_topic_radicals);
        Button pitch_button = findViewById(R.id.help_topic_pitch);

        pitch_button.setOnClickListener(getOpenListener(HelpPitchActivity.class));
        romaji_button.setOnClickListener(getOpenListener(HelpRomajiActivity.class));
        radical_button.setOnClickListener(getOpenListener(HelpRadicalActivity.class));
    }

    private<T> View.OnClickListener getOpenListener(final Class<T> c) {
        return v -> {
            Intent intent = new Intent(HelpActivity.this, c);
            startActivity(intent);
        };
    }
}
