package eu.lepiller.nani.result;

import android.graphics.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class KanjiResult {
    public static class Sense {
        private final String lang, content;
        public Sense(String lang, String content) {
            this.lang = lang;
            this.content = content;
        }

        public String getContent() {
            return content;
        }

        public String getLang() {
            return lang;
        }
    }

    public static class Stroke {
        private final float numX, numY;
        private final int size;
        private final Path path;

        public Stroke(Path path, int size, float numX, float numY) {
            this.path = path;
            this.size = size;
            this.numX = numX;
            this.numY = numY;
        }

        public Path getPath() {
            return path;
        }

        public float getNumX() {
            return numX;
        }

        public float getNumY() {
            return numY;
        }

        public int getSize() {
            return size;
        }
    }

    private final String kanji;
    private int stroke;
    private final List<Sense> senses = new ArrayList<>();
    private final List<String> on = new ArrayList<>();
    private final List<String> kun = new ArrayList<>();
    private final List<String> nanori = new ArrayList<>();
    private final List<String> elements = new ArrayList<>();
    private final List<Stroke> strokes = new ArrayList<>();

    public KanjiResult(String kanji, int stroke, List<Sense> senses, List<String> kun, List<String> on, List<String> nanori,
                       List<String> elements, List<Stroke> strokes) {
        this.kanji = kanji;
        this.stroke = stroke;
        if(senses != null)
            this.senses.addAll(senses);
        if(kun != null)
            this.kun.addAll(kun);
        if(on != null)
            this.on.addAll(on);
        if(nanori != null)
            this.nanori.addAll(nanori);
        if(elements != null)
            this.elements.addAll(elements);
        if(strokes != null)
            this.strokes.addAll(strokes);
    }

    public <T> void addUniq(List<T> a, List<T> b) {
        for(T t: b) {
            if(a.contains(t))
                continue;
            a.add(t);
        }
    }

    public void merge(KanjiResult other) {
        this.senses.addAll(other.senses);
        addUniq(this.on, other.on);
        addUniq(this.kun, other.kun);
        addUniq(this.nanori, other.nanori);
        addUniq(this.elements, other.elements);

        this.senses.addAll(other.senses);
        if(this.stroke < 0)
            this.stroke = other.stroke;

        // Arbitrarily settle for the first one that has stroke order information, we can't mix multiple orders
        if(this.strokes.isEmpty()) {
            this.strokes.addAll(other.strokes);
        }
    }

    public List<Sense> getSenses() {
        return senses;
    }

    public int getStroke() {
        return stroke;
    }

    public List<String> getElements() {
        return elements;
    }

    public List<Stroke> getStrokes() {
        return strokes;
    }

    public List<String> getKun() {
        return kun;
    }

    public List<String> getNanori() {
        return nanori;
    }

    public List<String> getOn() {
        return on;
    }

    public String getKanji() {
        return kanji;
    }
}
