package eu.lepiller.nani.result;

import android.os.Build;
import android.util.Log;

import com.moji4j.MojiConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Character.UnicodeBlock.*;

public class Result {
    public static class Source {
        private final List<String> content;
        private final boolean wasei;
        private final String language;

        public Source(List<String> content, boolean wasei, String language) {
            this.content = content;
            this.wasei = wasei;
            this.language = language;
        }
    }

    public static class Sense {
        private final List<String> references, limits, infos, glosses;
        private final String language;
        private final List<Source> sources;

        public Sense(List<String> references, List<String> limits, List<String> infos,
                     List<Source> sources, List<String> glosses,
                     String language) {
            this.references = references;
            this.limits = limits;
            this.infos = infos;
            this.sources = sources;
            this.glosses = glosses;
            this.language = language;
        }

        public List<String> getGlosses() {
            return glosses;
        }

        public List<String> getInfos() {
            return infos;
        }

        public String getLanguage() {
            return language;
        }
    }

    public static class Reading {
        private final List<String> kanjis, infos, readings, pitches;

        public Reading(List<String> kanjis, List<String> infos, List<String> readings) {
            this.kanjis = kanjis;
            this.infos = infos;
            this.readings = readings;
            this.pitches = new ArrayList<>();
        }

        public List<String> getKanjis() {
            return kanjis;
        }

        public List<String> getReadings() {
            return readings;
        }

        public void addPitch(String pitch) {
            pitches.add(pitch);
        }
    }

    private final List<String> kanjis;
    private final List<Reading> readings;
    private final List<Sense> senses;
    private final int score;

    public Result(List<String> kanjis, List<Reading> readings, List<Sense> senses, int score) {
        this.kanjis = kanjis;
        this.readings = readings;
        this.senses = senses;
        this.score = score;
    }

    public String getKanji() {
        String k = getReading();
        if(kanjis.size() > 0)
            k = kanjis.get(0);
        return k;
    }

    public List<String> getAlternatives() {
        return kanjis;
    }

    public List<Sense> getSenses() {
        return senses;
    }

    public List<Reading> getReadings() {
        return readings;
    }

    public String getReading() {
        String reading = "";
        if(readings.size() > 0) {
            List<String> rs = readings.get(0).readings;
            if(rs.size() > 0)
                reading = rs.get(0);
        }
        return reading;
    }

    public String getRomajiReading() {
        String reading = getReading();
        MojiConverter converter = new MojiConverter();
        return converter.convertKanaToRomaji(reading);
    }

    public String getPitch() {
        if(readings.size() > 0) {
            List<String> pitches = readings.get(0).pitches;
            if(pitches.size() > 0)
                return pitches.get(0);
        }
        return null;
    }

    private static boolean isKanji(char c) {
       Character.UnicodeBlock b = Character.UnicodeBlock.of(c);
       return b == CJK_UNIFIED_IDEOGRAPHS ||
              b == CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A ||
              b == CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B ||
              (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && b == CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C) ||
              (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && b == CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D) ||
              b == CJK_SYMBOLS_AND_PUNCTUATION;
    }


    public String getKanjiFurigana() {
        String txt = getKanji();
        String reading = getReading();
        Log.v("RESULT", "reading: " + reading);

        // split the text into kanji / not kanji portions
        ArrayList<String> portions = new ArrayList<>();

        StringBuilder current = new StringBuilder();
        Character.UnicodeBlock b = CJK_UNIFIED_IDEOGRAPHS;

        MojiConverter converter = new MojiConverter();
        for(int i=0; i<txt.length(); i++) {
            Character.UnicodeBlock b2 = Character.UnicodeBlock.of(txt.charAt(i));
            if(b != b2) {
                String s = current.toString();
                if(!s.isEmpty())
                    portions.add(s);
                current = new StringBuilder();
            }
            current.append(txt.charAt(i));

            b = b2;
        }
        String str = current.toString();
        if(!str.isEmpty())
            portions.add(str);

        // Create a regexp to match kanji places
        current = new StringBuilder();
        current.append("^");
        boolean wasKanji = false;
        for(String s: portions) {
            if(isKanji(s.charAt(0))) {
                if(!wasKanji)
                    current.append("(.*)");
                wasKanji = true;
            } else {
                for(Character c: s.toCharArray()) {
                    if(Character.UnicodeBlock.of(c) == KATAKANA) {
                        current.append("[");
                        current.append(c);
                        current.append(converter.convertRomajiToHiragana(converter.convertKanaToRomaji(String.valueOf(c))));
                        current.append("]");
                    } else {
                        current.append(c);
                    }
                }
                wasKanji = false;
            }
        }
        current.append("$");

        Log.v("RESULT", "regex: " + current);

        Pattern p = Pattern.compile(current.toString());
        Matcher m = p.matcher(reading);

        if(!m.matches()) {
            Log.v("RESULT", "Finally: " + txt);
            return txt;
        }

        // We have a match!

        Log.v("RESULT", "matched");

        current = new StringBuilder();
        int group = 1;
        wasKanji = false;
        for(String s: portions) {
            if(isKanji(s.charAt(0))) {
                current.append(s);
                wasKanji = true;
            } else {
                if(wasKanji) {
                    current.append("|");
                    current.append(m.group(group));
                    current.append(" ");
                    group++;
                }
                current.append(s);
                current.append(" ");
                wasKanji = false;
            }
        }
        if(wasKanji) {
            current.append("|");
            current.append(m.group(group));
            current.append(" ");
        }
        Log.v("RESULT", "Finaly: " + current);
        return current.toString();
    }

    private static void restrictResult(Result other) {
        for(Sense s: other.senses) {
            boolean hasReadingLimits = false;
            boolean hasKanjiLimits = false;
            for(String l: s.limits) {
                if(other.kanjis.contains(l))
                    hasKanjiLimits = true;
                for(Reading r: other.readings) {
                    if (r.readings.contains(l)) {
                        hasReadingLimits = true;
                        break;
                    }
                }
            }
            if(!hasKanjiLimits) {
                s.limits.addAll(other.kanjis);
            }
            if(!hasReadingLimits) {
                for(Reading r: other.readings)
                    s.limits.addAll(r.readings);
            }
        }
    }

    public void merge(Result other) {
        restrictResult(other);
        restrictResult(this);
        // merge meanings and readings
        Log.d("MERGE", "score, other: " + other.score + ", this: " + this.score);
        if(other.score <= this.score) {
            this.readings.addAll(0, other.readings);
            this.senses.addAll(0, other.senses);
        } else {
            this.readings.addAll(other.readings);
            this.senses.addAll(other.senses);
        }

        // merge kanjis
        for(String ok: other.kanjis) {
            boolean here = false;
            for (String k: kanjis)
                if(k.compareTo(ok) == 0)
                    here = true;
            if(!here) {
                // Add it first if the other entry has higher score
                if(other.score < this.score)
                    kanjis.add(0, ok);
                else
                    kanjis.add(ok);
            }
        }
    }
}
