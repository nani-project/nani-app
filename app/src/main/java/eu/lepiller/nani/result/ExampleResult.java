package eu.lepiller.nani.result;

import java.util.List;

public class ExampleResult {
    private final String japanese, otherLang, lang;
    private final List<String> tags;
    private final byte[] audio;

    public ExampleResult(String japanese, String otherLang, String lang, List<String> tags, byte[] audio) {
        this.japanese = japanese;
        this.otherLang = otherLang;
        this.lang = lang;
        this.tags = tags;
        this.audio = audio;
    }

    public String getLang() {
        return lang;
    }

    public String getOtherLang() {
        return otherLang;
    }

    public String getJapanese() {
        return japanese;
    }

    public List<String> getTags() {
        return tags;
    }

    public byte[] getAudio() {
        return audio;
    }
}
