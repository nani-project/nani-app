package eu.lepiller.nani;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class SettingsActivity extends AppCompatActivity {
    public static final String KEY_PREF_RAD_SIZE = "rad_size";
    public static final String KEY_PREF_READING_STYLE = "reading_style";
    public static final String KEY_PREF_PITCH_STYLE = "pitch_style";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
