package eu.lepiller.nani;

import java.util.List;

import eu.lepiller.nani.dictionary.DictionaryException;
import eu.lepiller.nani.result.ExampleResult;
import eu.lepiller.nani.result.KanjiResult;
import eu.lepiller.nani.result.Result;

class SearchResult {
    private List<Result> results;
    private List<KanjiResult> kanjiResults;
    private List<ExampleResult> exampleResults;
    private DictionaryException exception;
    private boolean converted;
    private String text;

    SearchResult(List<Result> results, List<KanjiResult> kanjiResults, List<ExampleResult> exampleResults, String text, boolean converted) {
        this.results = results;
        this.kanjiResults = kanjiResults;
        this.exampleResults = exampleResults;
        this.converted = converted;
        this.text = text;
    }

    SearchResult(DictionaryException e) {
        exception = e;
    }

    boolean isException() {
        return exception != null;
    }

    DictionaryException getException() {
        return exception;
    }

    List<Result> getResults() {
        return results;
    }

    List<KanjiResult> getKanjiResults() {
        return kanjiResults;
    }

    List<ExampleResult> getExampleResults() {
        return exampleResults;
    }

    boolean isConverted() {
        return converted;
    }

    String getText() {
        return text;
    }
}
