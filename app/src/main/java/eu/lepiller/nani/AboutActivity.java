package eu.lepiller.nani;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Button sources_button = findViewById(R.id.sources_button);
        Button report_button = findViewById(R.id.report_button);
        Button mozc_button = findViewById(R.id.mozc_button);

        sources_button.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framagit.org/nani-project/nani-app"));
            startActivity(browserIntent);
        });

        report_button.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framagit.org/nani-project/nani-app/issues"));
            startActivity(browserIntent);
        });

        mozc_button.setOnClickListener(v -> {
            String appPackageName = "org.mozc.android.inputmethod.japanese";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        });
    }
}
