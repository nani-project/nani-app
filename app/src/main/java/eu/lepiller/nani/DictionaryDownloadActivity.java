package eu.lepiller.nani;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.content.ComponentName;

import eu.lepiller.nani.dictionary.Dictionary;
import eu.lepiller.nani.dictionary.DictionaryFactory;

public class DictionaryDownloadActivity extends AppCompatActivity {
    final static String EXTRA_DICTIONARY = "eu.lepiller.nani.extra.DICTIONARY";
    private final static String TAG = "DictionaryDownload";

    Dictionary d;

    ImageView download_button = null;
    ProgressBar download_bar;

    Observer<DictionaryDownloadService.DownloadData> observer;

    View.OnClickListener download_click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            NetworkRequest request = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED)
                    .build();

            JobInfo.Builder infoBuilder = getInfoBuilder(request);

            JobInfo info = infoBuilder.build();
            JobScheduler scheduler = (JobScheduler) getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
            scheduler.schedule(info);
        }

        private JobInfo.Builder getInfoBuilder(NetworkRequest request) {
            PersistableBundle bundle = new PersistableBundle();
            bundle.putString(EXTRA_DICTIONARY, d.getName());

            JobInfo.Builder infoBuilder = new JobInfo.Builder(1, new ComponentName(getApplicationContext(), DictionaryDownloadService.class))
                    .setExtras(bundle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                infoBuilder = infoBuilder.setUserInitiated(true);
                infoBuilder = infoBuilder.setRequiredNetwork(request);
            }
            return infoBuilder;
        }
    };

    View.OnClickListener pause_click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            JobScheduler scheduler = (JobScheduler) getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
            scheduler.cancel(1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_download);
        Bundle extras = getIntent().getExtras();

        String name = null;
        if(extras != null)
            name = extras.getString(EXTRA_DICTIONARY);

        d = DictionaryFactory.getByName(this, name);
        download_bar = findViewById(R.id.download_progress);
        download_button = findViewById(R.id.download_button);

        Log.d("DOWNLOAD_ACTIVITY", "dictionary " + name + " is " + (d.isDownloaded()? "downloaded": "not downloaded"));

        setResult(DictionaryActivity.DICO_REQUEST);

        updateLayout(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LiveData<DictionaryDownloadService.DownloadData> data = DictionaryDownloadService.getData();
        final String dictionaryName = d.getName();

        if (dictionaryName == null)
            return;

        observer = downloadData -> {
            boolean pending = false;
            for(String n: downloadData.downloading) {
                if(n.equals(dictionaryName)) {
                    pending = true;
                    break;
                }
            }
            Log.d(TAG, "onChanged: " + downloadData.currentName);

            if(dictionaryName.equals(downloadData.currentName)) {
                download_bar.setMax(100);
                if(downloadData.currentProgress >= 0) {
                    download_bar.setIndeterminate(false);
                    download_bar.setProgress(downloadData.currentProgress);
                } else {
                    download_bar.setIndeterminate(true);
                }
                updateLayout(true);
            } else if(pending) {
                download_bar.setIndeterminate(true);
                updateLayout(true);
            } else {
                if(d.isDownloaded()) {
                    download_bar.setProgress(100);
                } else {
                    download_bar.setProgress(d.getSize()*100 / d.getExpectedFileSize());
                }
                updateLayout(false);
            }
        };
        data.observe(this, observer);
    }

    @Override
    protected void onPause() {
        LiveData<DictionaryDownloadService.DownloadData> data = DictionaryDownloadService.getData();
        data.removeObserver(observer);
        super.onPause();
    }

    private void updateLayout(final boolean isDownloading) {
        Log.d(TAG, "updateLayout: " + isDownloading);
        TextView name_view = findViewById(R.id.name_view);
        name_view.setText(d.getName());

        TextView description_view = findViewById(R.id.additional_info_view);
        description_view.setText(d.getDescription());
        TextView full_description_view = findViewById(R.id.extended_info_view);
        full_description_view.setText(d.getFullDescription());

        ImageView icon_view = findViewById(R.id.icon_view);
        Drawable icon = d.getDrawable(getApplicationContext());
        if (icon != null) {
            icon_view.setImageDrawable(icon);
        }

        int drawableResId;
        if(isDownloading) {
            drawableResId = R.drawable.ic_pause;
            download_button.setContentDescription(getString(R.string.alt_text_pause));
            download_button.setOnClickListener(pause_click_listener);
        } else {
            if(d.isDownloaded()) {
                drawableResId = R.drawable.ic_nani_refresh;
                download_button.setContentDescription(getString(R.string.alt_text_refresh));
            } else {
                drawableResId = R.drawable.ic_nani_download;
                download_button.setContentDescription(getString(R.string.alt_text_download));
            }
            download_button.setOnClickListener(download_click_listener);
        }

        setIcon(download_button, drawableResId);
        download_button.setEnabled(true);

        drawableResId = R.drawable.ic_nani_trash;
        ImageView trash_button = findViewById(R.id.remove_button);
        setIcon(trash_button, drawableResId);

        LinearLayout remove_layout = findViewById(R.id.remove_layout);
        remove_layout.setVisibility(d.getSize() > 0 && !isDownloading? View.VISIBLE: View.INVISIBLE);

        TextView size_view = findViewById(R.id.size_view);
        int size = d.getSize();
        if(size < 1500)
            size_view.setText(String.format(getResources().getString(R.string.dictionary_size_b), size));
        else if(size < 1500000)
            size_view.setText(String.format(getResources().getString(R.string.dictionary_size_kb), size/1000));
        else
            size_view.setText(String.format(getResources().getString(R.string.dictionary_size_mb), size/1000000));

        trash_button.setOnClickListener(v -> {
            d.remove();
            updateLayout(isDownloading);
        });
    }

    private void setIcon(ImageView download_button, int drawableResId) {
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), drawableResId, getTheme());
        download_button.setImageDrawable(drawable);
    }
}
