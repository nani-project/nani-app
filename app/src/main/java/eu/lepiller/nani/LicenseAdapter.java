package eu.lepiller.nani;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class LicenseAdapter extends BaseExpandableListAdapter {
    Context context;
    List<LicenseActivity.License> licenses;

    LicenseAdapter(Context context, List<LicenseActivity.License> licenses) {
        this.licenses = licenses;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return licenses.size();
    }

    @Override
    public int getChildrenCount(int i) {
        List<LicenseActivity.License> projects = licenses.get(i).getSubProjects();
        if(projects == null || projects.size() == 0)
            return 1;
        return projects.size() + 1;
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.layout_license_name, viewGroup, false);
        }
        String name = licenses.get(i).getName();
        Drawable icon = licenses.get(i).getDrawable();
        TextView nameView = (TextView) view.findViewById(R.id.license_name);
        ImageView iconView = (ImageView) view.findViewById(R.id.icon_view);
        ImageView arrowView = (ImageView) view.findViewById(R.id.arrow_view);
        nameView.setText(name);
        iconView.setImageDrawable(icon);
        arrowView.setImageResource(b? android.R.drawable.arrow_up_float: android.R.drawable.arrow_down_float);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        List<LicenseActivity.License> projects = licenses.get(i).getSubProjects();
        if(projects != null && projects.size() > 0)
            i1 -= 1;
        if(i1 < 0) {
            if(view == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = layoutInflater.inflate(R.layout.layout_license, viewGroup, false);
            }

            TextView nameView = view.findViewById(R.id.name);
            TextView descriptionView = view.findViewById(R.id.description);
            TextView licenseView = view.findViewById(R.id.license);
            Button projectButton = view.findViewById(R.id.website_button);

            descriptionView.setText(licenses.get(i).getDescription());
            nameView.setVisibility(View.GONE);
            licenseView.setVisibility(View.GONE);
            projectButton.setVisibility(View.GONE);
            return view;
        } else {
            boolean subproject = projects != null && projects.size() > 0;
            LicenseActivity.License license = subproject? projects.get(i1): licenses.get(i);

            if(view == null || i1 == 0) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = layoutInflater.inflate(R.layout.layout_license, viewGroup, false);
            }

            TextView nameView = view.findViewById(R.id.name);
            TextView descriptionView = view.findViewById(R.id.description);
            TextView licenseView = view.findViewById(R.id.license);
            Button projectButton = view.findViewById(R.id.website_button);

            nameView.setVisibility(subproject? View.VISIBLE: View.GONE);
            projectButton.setVisibility(license.getUrl() == null? View.GONE: View.VISIBLE);
            licenseView.setVisibility(View.VISIBLE);

            if(license.getUrl() != null)
                projectButton.setOnClickListener(view1 -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(license.getUrl()));
                    context.startActivity(browserIntent);
                });

            nameView.setText(license.getName());
            descriptionView.setText(license.getDescription());
            licenseView.setText(license.getLicense());

            return view;
        }
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
