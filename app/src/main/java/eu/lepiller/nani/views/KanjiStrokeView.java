package eu.lepiller.nani.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.KanjiResult;

public class KanjiStrokeView extends View {
    public enum ColorScheme {
        NOCOLOR, HIGHCONTRAST, SPECTRUM
    }

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Path path = new Path();
    private final Matrix matrix = new Matrix();
    private final RectF rectF = new RectF();

    private ColorScheme scheme = ColorScheme.SPECTRUM;
    private final List<KanjiResult.Stroke> strokes = new ArrayList<>();
    private int defaultColor = Color.BLACK;
    private int size = 64;

    public KanjiStrokeView(Context context) {
        super(context);
        setDefaults();
    }

    public KanjiStrokeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setDefaults();
        setupAttributes(attrs, 0);
    }

    public KanjiStrokeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setDefaults();
        setupAttributes(attrs, defStyleAttr);
    }

    private void setDefaults() {
        defaultColor = getContext().getResources().getColor(android.R.color.tab_indicator_text);
    }

    public void setupAttributes(AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.KanjiStrokeView, defStyleAttr, 0);
        defaultColor = typedArray.getColor(R.styleable.KanjiStrokeView_defaultColor, defaultColor);
        size = typedArray.getDimensionPixelSize(R.styleable.KanjiStrokeView_size, size);
    }

    public void setScheme(ColorScheme scheme) {
        this.scheme = scheme;
    }

    public void setStrokes(List<KanjiResult.Stroke> strokes) {
        this.strokes.clear();
        this.strokes.addAll(strokes);
    }

    private static int getColor(int i, int m, ColorScheme scheme, int defaultColor) {
        if(scheme == ColorScheme.NOCOLOR)
            return defaultColor;

        float angle = 0.618033988749895f;
        float[] hsv = new float[3];
        if(scheme == ColorScheme.SPECTRUM)
            hsv[0] = 360f * ((float)i / m);
        else if(scheme == ColorScheme.HIGHCONTRAST) {
            hsv[0] = 360f * angle * i;
            while(hsv[0] > 360f)
                hsv[0] -= 360f;
        }
        hsv[1] = 0.95f;
        hsv[2] = 0.75f;

        return Color.HSVToColor(hsv);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int color;
        int i = 0;
        int m = strokes.size();
        paint.setTextSize((float)size/12);
        for(KanjiResult.Stroke s: strokes) {
            color = getColor(i, m, scheme, defaultColor);
            paint.setColor(color);

            // Draw number
            paint.setStrokeWidth(2);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            canvas.drawText(String.valueOf(i+1),s.getNumX() / 109 * size, s.getNumY() / 109 * size, paint);

            // Draw stroke
            path.set(s.getPath());
            path.computeBounds(rectF, true);
            matrix.setScale((float)size / s.getSize(), (float)size / s.getSize(), 0, 0);
            path.transform(matrix);

            paint.setStrokeWidth((float)size/32);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(path, paint);

            i++;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(size, size);
    }
}
