package eu.lepiller.nani.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Pair;

import androidx.annotation.Nullable;

public class PitchDiagramView extends PitchView {
    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public PitchDiagramView(Context context) {
        super(context);
    }

    public PitchDiagramView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PitchDiagramView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private static float getCharWidth(float textSize) {
        return (float)2.25 * textSize;
    }

    private static float getPitchRadius(float textSize) {
        return textSize / 3;
    }

    private static float getStrokeWidth(float textSize) {
        return getPitchRadius(textSize) / 2;
    }

    private static float getParticleRadius(float textSize) {
        return 2 * getPitchRadius(textSize) / 3;
    }

    private static float getLowPosition(float textSize) {
        return 2 * textSize + getParticleRadius(textSize);
    }

    private static float getHighPosition(float textSize) {
        return getPitchRadius(textSize) + getParticleRadius(textSize);
    }

    private static float getMoraPosition(float textSize) {
        return (float)3.5 * textSize + getParticleRadius(textSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(pitchedMora == null || pitchedMora.size() == 0)
            return;

        paint.setStrokeWidth(getParticleRadius(textSize));
        paint.setTextSize(textSize);
        boolean previous = true;

        for(int i = 0; i< pitchedMora.size(); i++) {
            paint.setColor(pitchColor);
            Pair<String, Boolean> mora = pitchedMora.get(i);
            paint.setStyle(mora.first==null? Paint.Style.STROKE: Paint.Style.FILL_AND_STROKE);
            float x = getX(i, textSize);
            float y = getY(mora.second, textSize);
            canvas.drawCircle(x, y, getPitchRadius(textSize), paint);

            if(i>0) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(getStrokeWidth(textSize));
                double radius = getPitchRadius(textSize) + getParticleRadius(textSize)/2;
                drawLine(canvas, paint, i-1, previous, mora.second, radius, textSize);
                paint.setStrokeWidth(getParticleRadius(textSize));
            }

            if(mora.first != null) {
                paint.setColor(textColor);
                float width = paint.measureText(mora.first);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawText(mora.first, x - width/2, getMoraPosition(textSize), paint);
            }

            previous = mora.second;
        }
    }

    private static void drawLine(Canvas canvas, Paint paint, int position, boolean a, boolean b, double radius, float textSize) {
        float startX = getX(position, textSize);
        float startY = getY(a, textSize);
        float endX = getX(position + 1, textSize);
        float endY = getY(b, textSize);

        if(a == b) {
            startX += radius;
            endX -= radius;
        } else {
            double dist = Math.sqrt(Math.pow(startX-endX, 2)+Math.pow(startY-endY, 2));
            startY += (endY - startY) * radius / dist;
            startX += (endX - startX) * radius / dist;

            endX -= (endX - startX) * radius / dist;
            endY -= (endY - startY) * radius / dist;
        }

        canvas.drawLine(startX, startY, endX, endY, paint);
    }

    private static float getX(int i, float textSize) {
        return i*getCharWidth(textSize) + getCharWidth(textSize)/2;
    }

    private static float getY(boolean high, float textSize) {
        return high? getHighPosition(textSize): getLowPosition(textSize);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if(pitchedMora != null)
            setMeasuredDimension((int)getX(pitchedMora.size(), textSize), (int)(getMoraPosition(textSize) + textSize/2));
        else
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }
}
