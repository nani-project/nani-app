package eu.lepiller.nani.views;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import eu.lepiller.nani.R;

public abstract class PitchView extends View {
    private int accent;
    private String text;

    protected int textColor = Color.BLACK;
    protected int pitchColor = Color.BLACK;
    protected ArrayList<Pair<String, Boolean>> pitchedMora = new ArrayList<>();
    protected float textSize = 32;

    public PitchView(Context context) {
        super(context);
        setDefaults();
    }

    public PitchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setDefaults();
        setupAttributes(attrs, 0);
    }

    public PitchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setDefaults();
        setupAttributes(attrs, defStyleAttr);
    }

    private void setDefaults() {
        textColor = getContext().getResources().getColor(android.R.color.tab_indicator_text);
        int mode = getContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        if(mode == Configuration.UI_MODE_NIGHT_NO) {
            pitchColor = getContext().getResources().getColor(android.R.color.background_dark);
        } else if(mode == Configuration.UI_MODE_NIGHT_YES) {
            pitchColor = getContext().getResources().getColor(android.R.color.background_light);
        }
        textSize = new TextView(getContext()).getTextSize();
    }

    private void setupAttributes(AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PitchDiagramView, defStyleAttr, 0);
        textColor = typedArray.getColor(R.styleable.PitchDiagramView_textColor, textColor);
        pitchColor = typedArray.getColor(R.styleable.PitchDiagramView_pitchColor, pitchColor);
        textSize = typedArray.getDimension(R.styleable.PitchDiagramView_textSize, textSize);
        text = typedArray.getString(R.styleable.PitchDiagramView_text);
        accent = typedArray.getInt(R.styleable.PitchDiagramView_pitch, -1);
        updatePitchedMora();
    }

    protected static boolean is_mora(char c) {
        ArrayList<Character> not_mora = new ArrayList<>();
        not_mora.add("ゃ".charAt(0));
        not_mora.add("ゅ".charAt(0));
        not_mora.add("ょ".charAt(0));
        not_mora.add("ャ".charAt(0));
        not_mora.add("ュ".charAt(0));
        not_mora.add("ョ".charAt(0));
        return !not_mora.contains(c);
    }

    public void setText(String text) {
        this.text = text;
        updatePitchedMora();
    }

    public void setPitch(int pitch) {
        this.accent = pitch;
        updatePitchedMora();
    }

    public String getText() {
        return text;
    }

    public int getPitch() {
        return accent;
    }

    private void updatePitchedMora() {
        ArrayList<String> moras = new ArrayList<>();

        if(accent < 0 || text == null)
            return;

        for(int i=0; i<text.length(); i++) {
            char here = text.charAt(i);
            if(is_mora(here) && (i == text.length()-1 || is_mora(text.charAt(i+1))))
                moras.add(text.substring(i,i+1));
            else if(is_mora(here))
                moras.add(text.substring(i,i+2));
        }
        moras.add(null);
        pitchedMora = new ArrayList<>();
        if(accent == 0)
            setHeiban(pitchedMora, moras);
        else if(accent == 1)
            setAtamadaka(pitchedMora, moras);
        else
            setOtherAccent(pitchedMora, moras, accent);
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void setPitchColor(int pitchColor) {
        this.pitchColor = pitchColor;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    private static void setHeiban(ArrayList<Pair<String, Boolean>> text, ArrayList<String> moras) {
        text.add(new Pair<>(moras.get(0), false));
        for(int i=1; i<moras.size(); i++) {
            text.add(new Pair<>(moras.get(i), true));
        }
    }

    private static void setAtamadaka(ArrayList<Pair<String, Boolean>> text, ArrayList<String> moras) {
        text.add(new Pair<>(moras.get(0), true));
        for(int i=1; i<moras.size(); i++) {
            text.add(new Pair<>(moras.get(i), false));
        }
    }

    private static void setOtherAccent(ArrayList<Pair<String, Boolean>> text, ArrayList<String> moras, int accent) {
        text.add(new Pair<>(moras.get(0), false));
        int i;
        for(i=1; i<moras.size() && i < accent; i++) {
            text.add(new Pair<>(moras.get(i), true));
        }
        for(; i<moras.size(); i++) {
            text.add(new Pair<>(moras.get(i), false));
        }
    }
}
