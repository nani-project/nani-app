package eu.lepiller.nani.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Pair;

import androidx.annotation.Nullable;

public class PitchContourView extends PitchView {
    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final static int spacing = 4;

    public PitchContourView(Context context) {
        super(context);
    }

    public PitchContourView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PitchContourView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(pitchedMora == null || pitchedMora.size() == 0)
            return;

        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paint.setStrokeWidth(2);

        int x = 0;
        for(int i = 0; i< pitchedMora.size(); i++) {
            Pair<String, Boolean> mora = pitchedMora.get(i);
            float posX = getX(x, textSize);
            String text = mora.first == null? "☆": mora.first;
            x += text.length();
            float width = paint.measureText(text);

            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize((float)textSize);
            paint.setStrokeWidth(textSize);
            canvas.drawText(text, posX+spacing, textSize + textSize / 4, paint);

            paint.setStrokeWidth(2);
            paint.setStyle(Paint.Style.STROKE);
            if(mora.second) {
                canvas.drawLine(posX, 0, posX+width+2*spacing, 0, paint);
            } else {
                canvas.drawLine(posX, textSize + textSize/2, posX+width+2*spacing, textSize + textSize/2, paint);
            }

            if(i+1<pitchedMora.size()) {
                Pair<String, Boolean> mora2 = pitchedMora.get(i+1);
                if(mora2.second != mora.second) {
                    canvas.drawLine(posX+width+2*spacing, 0, posX+width+2*spacing, textSize + textSize/2, paint);
                }
            }
        }

    }

    private static float getX(int i, float textSize) {
        return i*textSize + i*2*spacing + textSize/2;
    }

    private int totalSize() {
        int total = 0;
        for(int i = 0; i< pitchedMora.size(); i++) {
            Pair<String, Boolean> mora = pitchedMora.get(i);
            if(mora.first != null)
                total += mora.first.length();
        }
        return total+1;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if(pitchedMora != null)
            setMeasuredDimension((int)getX(totalSize(), textSize), (int)(textSize + textSize/2));
        else
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }
}
