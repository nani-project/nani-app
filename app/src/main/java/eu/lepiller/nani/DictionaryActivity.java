package eu.lepiller.nani;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import eu.lepiller.nani.dictionary.DictionariesAdapter;
import eu.lepiller.nani.dictionary.DictionaryFactory;
import eu.lepiller.nani.dictionary.Dictionary;

public class DictionaryActivity extends AppCompatActivity {
    static final int DICO_REQUEST = 1;
    static final String TAG = "DICO_LIST";
    DictionariesAdapter adapter;
    ArrayAdapter<String> langAdapter;
    ArrayList<Dictionary> dictionaries;
    List<String> langs;
    String selectedLanguage = "";
    private SwipeRefreshLayout refresher;
    private DownloadThread downloadThread = new DownloadThread();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        final Spinner lang_view = findViewById(R.id.lang_view);
        final ListView list_view = findViewById(R.id.dictionary_view);
        refresher = findViewById(R.id.dictionary_refresh_layout);

        dictionaries = new ArrayList<>();
        dictionaries.addAll(DictionaryFactory.getDictionaries(getApplicationContext()));

        langs = new ArrayList<>();
        langs.add(getString(R.string.all_langs));
        langs.addAll(DictionaryFactory.getLangs(getApplicationContext()));

        adapter = new DictionariesAdapter(this, dictionaries);
        list_view.setAdapter(adapter);

        langAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, langs);
        langAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lang_view.setAdapter(langAdapter);

        if(dictionaries.isEmpty()) {
            Snackbar.make(findViewById(R.id.dictionary_view), getString(R.string.no_dico_list),
                    Snackbar.LENGTH_LONG).show();
            refresh();
        }

        ActivityResultLauncher<Intent> activity = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        updateDataSet();
                    }
                });

        list_view.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(DictionaryActivity.this, DictionaryDownloadActivity.class);
            intent.putExtra(DictionaryDownloadActivity.EXTRA_DICTIONARY, DictionaryFactory.get(position,
                    lang_view.getSelectedItemPosition() == 0? null: lang_view.getSelectedItem().toString()).getName());

            activity.launch(intent);
        });

        lang_view.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0)
                    selectedLanguage = "";
                else
                    selectedLanguage = parent.getItemAtPosition(position).toString();
                updateDataSet();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedLanguage = "";
                updateDataSet();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setObserver();
        refresher.setOnRefreshListener(this::refresh);
        updateDataSet();
    }

    @Override
    protected void onPause() {
        unsetObserver();
        super.onPause();
    }

    private void refresh() {
        downloadThread.start();
    }

    private void setObserver() {
        LiveData<Boolean> data = downloadThread.getRefreshingData();
        data.observe(this, refreshing -> {
            refresher.setRefreshing(refreshing);
            if(!refreshing) {
                downloadThread = new DownloadThread();
                setObserver();
                updateDataSet();
            }
        });
    }

    private void unsetObserver() {
        LiveData<Boolean> data = downloadThread.getRefreshingData();
        data.removeObservers(this);
    }

    private class DownloadThread extends Thread {
        final private MutableLiveData<Boolean> refreshing = new MutableLiveData<>();

        LiveData<Boolean> getRefreshingData() {
            return refreshing;
        }

        @Override
        public void run() {
            refreshing.postValue(true);
            update();
            refreshing.postValue(false);
        }

        private void update() {
            try {
                URL url = DictionaryFactory.getListUrl();
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream input = connection.getInputStream();
                    File file = DictionaryFactory.getListFile();
                    File parent = file.getParentFile();
                    if(parent != null && !parent.exists())
                        if(!parent.mkdirs())
                            Log.w(TAG, "Could not create parent directory for " + file);
                    int count;
                    byte[] data = new byte[4096];
                    FileOutputStream output = new FileOutputStream(file);
                    while((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                } else {
                    postResult(R.string.error_dico_not_found);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                postResult(R.string.error_dico_url);
            } catch (IOException e) {
                e.printStackTrace();
                postResult(R.string.error_dico_io);
            }
        }

        protected void postResult(Integer i) {
            runOnUiThread(() -> postMessage(i));
        }
    }

    private void postMessage(Integer i) {
        Snackbar.make(findViewById(R.id.dictionary_view), getString(i), Snackbar.LENGTH_LONG).show();
        refresh();
    }

    private void updateDataSet() {
        DictionaryFactory.updatePackageList();
        dictionaries.clear();
        dictionaries.addAll(DictionaryFactory.getDictionaries(getApplicationContext(), selectedLanguage));
        langs.clear();
        langs.add(getString(R.string.all_langs));
        langs.addAll(DictionaryFactory.getLangs(getApplicationContext()));
        adapter.notifyDataSetChanged();
        langAdapter.notifyDataSetChanged();
    }
}
