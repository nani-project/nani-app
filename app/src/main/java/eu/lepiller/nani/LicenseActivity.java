package eu.lepiller.nani;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.List;

public class LicenseActivity extends AppCompatActivity {
    ExpandableListAdapter adapter;

    public static class License {
        private final String name, description, license, url;
        private final List<License> subProjects;
        private final Drawable drawable;

        License(String name, Drawable drawable, String url, String description, String license, List<License> subProjects) {
            this.name = name;
            this.drawable = drawable;
            this.url = url;
            this.description = description;
            this.license = license;
            this.subProjects = subProjects;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getLicense() {
            return license;
        }

        @Nullable
        public List<License> getSubProjects() {
            return subProjects;
        }

        public Drawable getDrawable() {
            return drawable;
        }

        public String getUrl() {
            return url;
        }
    }

    private static final List<License> licenseInformation = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);

        licenseInformation.clear();
        List<License> erdrgLicenses = new ArrayList<>();
        erdrgLicenses.add(new License(getString(R.string.jmdict_title), null, "http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project",
                getString(R.string.jmdict_descr), getString(R.string.jmdict_license), null));
        erdrgLicenses.add(new License(getString(R.string.jmnedict_title), null, "https://www.edrdg.org/enamdict/enamdict_doc.html",
                getString(R.string.jmnedict_descr), getString(R.string.jmnedict_license), null));
        erdrgLicenses.add(new License(getString(R.string.kanjidic_title), null, "http://www.edrdg.org/wiki/index.php/KANJIDIC_Project",
                getString(R.string.kanjidic_descr), getString(R.string.kanjidic_license), null));
        erdrgLicenses.add(new License(getString(R.string.radk_title), null, "http://www.edrdg.org/krad/kradinf.html",
                getString(R.string.radk_descr), getString(R.string.radk_license), null));
        licenseInformation.add(
                new License(getString(R.string.erdrg_title), ResourcesCompat.getDrawable(getResources(), R.drawable.ic_nani_edrdg, getTheme()), null,
                        getString(R.string.erdrg_descr), "", erdrgLicenses));

        licenseInformation.add(
                new License(getString(R.string.jibiki_title), ResourcesCompat.getDrawable(getResources(), R.drawable.ic_jibiki, getTheme()), "https://jibiki.fr",
                        getString(R.string.jibiki_descr), getString(R.string.jibiki_license), null)
        );
        licenseInformation.add(
                new License(getString(R.string.kanjivg_title), ResourcesCompat.getDrawable(getResources(), R.drawable.ic_kanjivg, getTheme()), "http://kanjivg.tagaini.net/",
                        getString(R.string.kanjivg_descr), getString(R.string.kanjivg_license), null)
        );
        licenseInformation.add(
                new License(getString(R.string.tatoeba_title), ResourcesCompat.getDrawable(getResources(), R.drawable.ic_tatoeba, getTheme()), "https://tatoeba.org",
                        getString(R.string.tatoeba_descr), getString(R.string.tatoeba_license), null)
        );
        licenseInformation.add(
                new License(getString(R.string.wadoku_title), ResourcesCompat.getDrawable(getResources(), R.drawable.wadoku, getTheme()), "https://wadoku.de",
                        getString(R.string.wadoku_descr), getString(R.string.wadoku_license), null)
        );

        final ExpandableListView list_view = findViewById(R.id.license_view);
        adapter = new LicenseAdapter(this, licenseInformation);
        list_view.setAdapter(adapter);
    }
}
