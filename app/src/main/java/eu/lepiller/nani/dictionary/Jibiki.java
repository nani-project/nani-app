package eu.lepiller.nani.dictionary;

import java.io.File;

import eu.lepiller.nani.R;

public class Jibiki extends ResultDictionary {
    Jibiki(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    int getDrawableId() {
        return R.drawable.ic_jibiki;
    }
}
