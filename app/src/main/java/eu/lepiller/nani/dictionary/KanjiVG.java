package eu.lepiller.nani.dictionary;

import android.graphics.Path;
import android.util.Log;
import android.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.KanjiResult;

public class KanjiVG extends KanjiDictionary {
    final private static String TAG = "KANJIVG";
    private Huffman commandHuffman;

    KanjiVG(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    int getDrawableId() {
        return R.drawable.ic_kanjivg;
    }

    @Override
    public void remove() {
        super.remove();
        commandHuffman = null;
    }

    @Override
    KanjiResult search(String kanji) throws IncompatibleFormatException {
        if(isDownloaded()) {
            try {
                Log.d(TAG, "search for kanji " + kanji);
                RandomAccessFile file = new RandomAccessFile(getFile(), "r");
                byte[] header = new byte[15];
                int l = file.read(header);
                if (l != header.length)
                    return null;

                // Check file format version
                if (!Arrays.equals(header, "NANI_KANJIVG001".getBytes())) {
                    StringBuilder error = new StringBuilder("search: incompatible header: [");
                    boolean first = true;
                    for (byte b : header) {
                        if (first)
                            first = false;
                        else
                            error.append(", ");
                        error.append(b);
                    }
                    error.append("].");
                    Log.d(TAG, error.toString());
                    throw new IncompatibleFormatException(getName());
                }

                Log.d(TAG, "header OK");

                byte[] search = kanji.toLowerCase().getBytes();
                file.skipBytes(4); // size
                commandHuffman = new HuffmanParser().parse(file);
                long kanjiTriePos = file.getFilePointer();

                Log.d(TAG, "trie pos: " + kanjiTriePos);

                List<KanjiResult> results = searchTrie(file, kanjiTriePos, search, 1, new SingleTrieParser<KanjiResult>(new ResultParser(kanji)) {
                    @Override
                    public void skipVals(RandomAccessFile file1, long pos) throws IOException {
                        file1.seek(pos);
                        file1.skipBytes(4);
                    }

                    @Override
                    void seek(RandomAccessFile file, long pos) throws IOException {
                        file.seek(pos);
                        file.seek(file.readInt());
                    }
                });
                if(results.isEmpty())
                    return null;
                return results.get(0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static class ParseException extends Exception {
        ParseException() {}
    }

    private static Pair<Integer, Float> parsePathFloat(String command, int pos) {
        boolean negative = false;
        int integerPart = 0;
        int fractionPart = 0;
        int fractionSize = 1;
        boolean foundDot = false;

        if(command.charAt(pos) == '-') {
            negative = true;
            pos++;
        }

        while(pos < command.length()) {
            char c = command.charAt(pos);
            if(c == '.') {
                if (foundDot)
                    break;
                foundDot = true;
            } else if(c >= '0' && c <= '9') {
                if(foundDot) {
                    fractionPart *= 10;
                    fractionPart += c - '0';
                    fractionSize *= 10;
                } else {
                    integerPart *= 10;
                    integerPart += c - '0';
                }
            } else {
                break;
            }
            pos++;
        }

        float number = (float) integerPart + ((float) fractionPart / (float)fractionSize);
        return negative? new Pair<>(pos, -number): new Pair<>(pos, number);
    }

    private static Pair<Integer, List<Float>> parsePathFloatList(String command, int pos) {
        char c = command.charAt(pos);
        List<Float> res = new ArrayList<>();
        while((c >= '0' && c <= '9') || c == '.' || c == '-') {
            Pair<Integer, Float> f = parsePathFloat(command, pos);
            pos = f.first;
            res.add(f.second);
            if(pos == command.length())
                break;
            if(command.charAt(pos) == ',' || command.charAt(pos) == ' ')
                pos++;
            c = command.charAt(pos);
        }
        return new Pair<>(pos, res);
    }

    private static void parsePath(Path path, String command) throws ParseException {
        int pos = 0;
        float lastX = 0; float lastY = 0;
        float lastX1 = 0; float lastY1 = 0;
        float x1, y1;
        Pair<Integer, List<Float>> lst;
        while(pos < command.length()) {
            char com = command.charAt(pos);
            boolean relative = com < 'A' || com > 'Z';
            pos++;
            switch(com) {
                case 'm':
                case 'M':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 2)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastX += lst.second.get(0);
                        lastY += lst.second.get(1);
                    } else {
                        lastX = lst.second.get(0);
                        lastY = lst.second.get(1);
                    }
                    path.moveTo(lastX, lastY);
                    break;
                case 'c':
                case 'C':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 6)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastX1 = lastX + lst.second.get(2);
                        lastY1 = lastY + lst.second.get(3);
                        x1 = lastX + lst.second.get(0);
                        y1 = lastY + lst.second.get(1);
                        lastX += lst.second.get(4);
                        lastY += lst.second.get(5);
                    } else {
                        x1 = lst.second.get(0);
                        y1 = lst.second.get(1);
                        lastX1 = lst.second.get(2);
                        lastY1 = lst.second.get(3);
                        lastX = lst.second.get(4);
                        lastY = lst.second.get(5);
                    }
                    path.cubicTo(x1, y1, lastX1, lastY1, lastX, lastY);
                    break;
                case 's':
                case 'S':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 4)
                        throw new ParseException();
                    pos = lst.first;
                    x1 = 2*lastX - lastX1;
                    y1 = 2*lastY - lastY1;
                    if(relative) {
                        lastX1 = lastX + lst.second.get(0);
                        lastY1 = lastY + lst.second.get(1);
                        lastX += lst.second.get(2);
                        lastY += lst.second.get(3);
                    } else {
                        lastX1 = lst.second.get(0);
                        lastY1 = lst.second.get(1);
                        lastX = lst.second.get(2);
                        lastY = lst.second.get(3);
                    }
                    path.cubicTo(x1, y1, lastX1, lastY1, lastX, lastY);
                    break;
                case 'l':
                case 'L':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 2)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastX += lst.second.get(0);
                        lastY += lst.second.get(1);
                    } else {
                        lastX = lst.second.get(0);
                        lastY = lst.second.get(1);
                    }
                    path.lineTo(lastX, lastY);
                    break;
                case 'h':
                case 'H':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 1)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastX += lst.second.get(0);
                    } else {
                        lastX = lst.second.get(0);
                    }
                    path.lineTo(lastX, lastY);
                    break;
                case 'v':
                case 'V':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 1)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastY += lst.second.get(0);
                    } else {
                        lastY = lst.second.get(0);
                    }
                    path.lineTo(lastX, lastY);
                    break;
                case 'q':
                case 'Q':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 4)
                        throw new ParseException();
                    pos = lst.first;
                    if(relative) {
                        lastX1 = lastX + lst.second.get(0);
                        lastY1 = lastY + lst.second.get(1);
                        lastX += lst.second.get(2);
                        lastY += lst.second.get(3);
                    } else {
                        lastX1 = lst.second.get(0);
                        lastY1 = lst.second.get(1);
                        lastX = lst.second.get(2);
                        lastY = lst.second.get(3);
                    }
                    path.quadTo(lastX1, lastY1, lastX, lastY);
                    break;
                case 't':
                case 'T':
                    lst = parsePathFloatList(command, pos);
                    if(lst.second.size() != 2)
                        throw new ParseException();
                    pos = lst.first;
                    lastX1 = 2*lastX - lastX1;
                    lastY1 = 2*lastY - lastY1;
                    if(relative) {
                        lastX += lst.second.get(0);
                        lastY += lst.second.get(1);
                    } else {
                        lastX = lst.second.get(0);
                        lastY = lst.second.get(1);
                    }
                    path.quadTo(lastX1, lastY1, lastX, lastY);
                    break;
                case 'z':
                case 'Z':
                    path.close();
                case ' ':
                    pos++;
                    break;
                default:
                    throw new ParseException();
            }
        }
    }

    class StrokeParser extends Parser<KanjiResult.Stroke> {
        @Override
        KanjiResult.Stroke parse(RandomAccessFile file) throws IOException {
            String command = new HuffmanStringParser(commandHuffman).parse(file);
            String x = new HuffmanStringParser(commandHuffman).parse(file);
            String y = new HuffmanStringParser(commandHuffman).parse(file);

            Path path = new Path();

            try {
                parsePath(path, command);
            } catch (ParseException e) {
                e.printStackTrace();
                path.reset();
            }

            return new KanjiResult.Stroke(path, 109, Float.parseFloat(x), Float.parseFloat(y));
        }
    }

    class ResultParser extends Parser<KanjiResult> {
        String kanji;
        ResultParser(String kanji) {
            this.kanji = kanji;
        }
        @Override
        KanjiResult parse(RandomAccessFile file) throws IOException {
            List<String> elements = new ListParser<>(new StringParser()).parse(file);
            List<KanjiResult.Stroke> strokes = new ListParser<>(new StrokeParser()).parse(file);

            return new KanjiResult(kanji, -1, null, null, null, null, elements, strokes);
        }
    }
}
