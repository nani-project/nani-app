package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.ExampleResult;

public class TatoebaDictionary extends ExampleDictionary {
    final private static String TAG = "TATOEBA";
    private Huffman japaneseHuffman, translationHuffman;

    TatoebaDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    int getDrawableId() {
        return R.drawable.ic_tatoeba;
    }

    private static class AudioParser extends Parser<byte[]> {
        @Override
        byte[] parse(RandomAccessFile file) throws IOException {
            int size = file.readShort();
            byte[] result = new byte[size];
            for(int i=0; i<size; i++)
                result[i] = file.readByte();
            return result;
        }
    }

    private class SentenceParser extends Parser<ExampleResult> {
        @Override
        ExampleResult parse(RandomAccessFile file) throws IOException {
            String japanese = new HuffmanStringParser(japaneseHuffman).parse(file);
            String translation = new HuffmanStringParser(translationHuffman).parse(file);
            List<String> tags = new ListParser<>(new HuffmanStringParser(translationHuffman)).parse(file);
            byte[] audio = new AudioParser().parse(file);

            return new ExampleResult(japanese, translation, TatoebaDictionary.this.getLang(), tags, audio);
        }
    }

    private static class ValuesParser extends Parser<List<Integer>> {
        @Override
        List<Integer> parse(RandomAccessFile file) throws IOException {
            ArrayList<Integer> results = new ArrayList<>();

            Log.v(TAG, "Getting values");
            List<Integer> exactResults = new ListParser<>(new Parser<Integer>() {
                @Override
                Integer parse(RandomAccessFile file) throws IOException {
                    return file.readInt();
                }
            }).parse(file);

            List<Integer> others = new ListParser<>(1, new Parser<Integer>() {
                @Override
                Integer parse(RandomAccessFile file) throws IOException {
                    file.skipBytes(1);
                    return file.readInt();
                }
            }).parse(file);

            for(Integer pos: others) {
                file.seek(pos);
                results.addAll(new ResultDictionary.ValuesParser().parse(file));
            }

            Collections.sort(results);
            Collections.sort(exactResults);

            Log.v(TAG, "exact result size: " + exactResults.size() + ", result size: " + results.size());
            Log.v(TAG, "exact: " + Arrays.toString(exactResults.toArray()) + ", others: " + Arrays.toString(results.toArray()));
            exactResults.addAll(results);
            return exactResults;
        }
    }

    private List<Integer> searchTrie(RandomAccessFile file, long triePos, byte[] txt) throws IOException {
        return searchTrie(file, triePos, txt, 50, new TrieParser<Integer>(new ValuesParser()) {
            @Override
            public void skipVals(RandomAccessFile file, long pos) throws IOException {
                file.seek(pos);
                int valuesLength = file.readShort();
                Log.v(TAG, "number of values: " + valuesLength);
                file.skipBytes(valuesLength * 4);
            }
        });
    }

    @Override
    List<ExampleResult> search(String word) throws IncompatibleFormatException {
        if (isDownloaded()) {
            try {
                RandomAccessFile file = new RandomAccessFile(getFile(), "r");
                byte[] header = new byte[16];
                int l = file.read(header);
                if (l != header.length)
                    return null;

                // Check file format version
                if (!Arrays.equals(header, "NANI_SENTENCE001".getBytes())) {
                    StringBuilder error = new StringBuilder("search: incompatible header: [");
                    boolean first = true;
                    for (byte b : header) {
                        if (first)
                            first = false;
                        else
                            error.append(", ");
                        error.append(b);
                    }
                    error.append("].");
                    Log.d(TAG, error.toString());
                    throw new IncompatibleFormatException(getName());
                }

                byte[] search = word.toLowerCase().getBytes();

                long triePos = file.readInt();

                Log.d(TAG, "Search in: " + getFile());
                Log.v(TAG, "trie: " + triePos);

                japaneseHuffman = new HuffmanParser().parse(file);
                translationHuffman = new HuffmanParser().parse(file);

                // Search in Japanese
                List<Integer> results = searchTrie(file, triePos, search);
                Log.d(TAG, results.size() + " result(s)");

                List<ExampleResult> r = new ArrayList<>();
                List<Integer> uniqResults = new ArrayList<>();
                for(Integer i: results) {
                    if(!uniqResults.contains(i))
                        uniqResults.add(i);
                }

                int num = 0;
                for(int pos: uniqResults) {
                    if(num > 10)
                        break;
                    num++;
                    file.seek(pos);
                    r.add(new SentenceParser().parse(file));
                }
                return r;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
