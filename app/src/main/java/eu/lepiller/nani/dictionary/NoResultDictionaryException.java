package eu.lepiller.nani.dictionary;

import java.util.ArrayList;
import java.util.List;

public class NoResultDictionaryException extends DictionaryException {
    private final ArrayList<String> tried;

    public NoResultDictionaryException(ArrayList<String> tried) {
        this.tried = tried;
    }

    public List<String> getTried() {
        return tried;
    }
}
