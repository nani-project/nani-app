package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.lepiller.nani.R;

public class RadicalDict extends FileDictionary {
    private final Map<String, Integer> strokeCount = new HashMap<>();
    private final Map<String, Integer> radicalStrokeCount = new HashMap<>();
    private final Map<String, List<String>> radicals = new HashMap<>();

    final private static String TAG = "RADK";

    @Override
    int getDrawableId() {
        return R.drawable.ic_nani_edrdg;
    }

    @Override
    public void remove() {
        super.remove();
        strokeCount.clear();
        radicalStrokeCount.clear();
        radicals.clear();
    }

    private void fillStrokeCount(RandomAccessFile file) throws IOException{
        int size = file.readShort();
        for(int i=0; i<size; i++) {
            String kanji = new StringParser().parse(file);
            int stroke = file.readByte();
            strokeCount.put(kanji, stroke);
        }
    }

    private void fillRadicalStrokeCount(RandomAccessFile file) throws IOException {
        int size = file.readShort();
        for(int i=0; i<size; i++) {
            String radical = new StringParser().parse(file);
            int stroke = file.readByte();

            radicalStrokeCount.put(radical, stroke);
        }
    }

    private void fillRadicals(RandomAccessFile file) throws IOException {
        int size = file.readShort();
        for(int i=0; i<size; i++) {
            String radical = new StringParser().parse(file);
            String kanji = new StringParser().parse(file);
            List<String> lst = new ArrayList<>();
            for(String s: kanji.split("")) {
                if(s.compareTo("") != 0) {
                    lst.add(s);
                }
            }

            radicals.put(radical, lst);
        }
    }

    private void fillTables() throws IncompatibleFormatException {
        try {
            RandomAccessFile file = new RandomAccessFile(getFile(), "r");
            byte[] header = new byte[12];
            int l = file.read(header);
            if (l != header.length)
                return;

            // Check file format version
            if(!Arrays.equals(header, "NANI_RADK001".getBytes()))
                throw new IncompatibleFormatException(getName());

            if (strokeCount.size() == 0) {
                fillRadicals(file);
                fillRadicalStrokeCount(file);
                fillStrokeCount(file);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    RadicalDict(String name, String description, String fullDescription, File cacheDir, File dataDir, String url,
                int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    private static boolean contains(List<String> lst, String s) {
        if(lst==null)
            return false;
        for(String p: lst) {
            if(p.compareTo(s) == 0) {
                return true;
            }
        }
        return false;
    }

    public Map<Integer, List<String>> match(List<String> rad) throws IncompatibleFormatException {
        if(! isDownloaded())
            return null;

        fillTables();
        Map<Integer, List<String>> result = null;
        for(String r: rad) {
            List<String> possibilities = radicals.get(r);
            if(possibilities == null)
                continue;
            if(result == null) {
                result = new HashMap<>();
                for(String kanji: possibilities) {
                    Integer strokes = strokeCount.get(kanji);
                    int stroke = 0;
                    if(strokes != null)
                        stroke = strokes;
                    Log.d(TAG, Integer.toString(stroke));
                    List <String> lst = result.get(stroke);
                    if(lst == null) {
                        lst = new ArrayList<>();
                        result.put(stroke, lst);
                    }
                    lst.add(kanji);
                }
            } else {
                for(List<String> kanjiList: result.values()) {
                    List<String> lst = new ArrayList<>(kanjiList);
                    for(String kanji: lst) {
                        if(! contains(possibilities, kanji))
                            kanjiList.remove(kanji);
                    }
                }
            }
        }

        if(result != null) {
            Set<Integer> strokes = new HashSet<>(result.keySet());
            for (int stroke : strokes) {
                List<String> kanji = result.get(stroke);
                if (kanji == null || kanji.size() == 0) {
                    result.remove(stroke);
                }
            }
        }

        return result;
    }

    public Set<String> availableRadicals(List<String> rad) throws IncompatibleFormatException {
        if(! isDownloaded())
            return null;

        fillTables();
        if(rad.size() == 0)
            return radicals.keySet();

        Set<String> result = new HashSet<>();
        Map<Integer, List<String>> kanjiList = match(rad);
        for(Map.Entry<String, List<String>> e: radicals.entrySet()) {
            String radical = e.getKey();
            List<String> possibilities = e.getValue();
            boolean possible = false;
            for(List<String> kanjiGroup: kanjiList.values()) {
                if(possible)
                    break;
                for(String s: kanjiGroup) {
                    if(contains(possibilities, s)) {
                        possible = true;
                        break;
                    }
                }
            }
            if(possible)
                result.add(radical);
        }
        return result;
    }

    public Map<Integer, List<String>> getAllRadicals() throws IncompatibleFormatException {
        if(! isDownloaded())
            return null;

        fillTables();

        Map<Integer, List<String>> result = new HashMap<>();
        for(String radical: radicals.keySet()) {
            int stroke = 0;
            Integer strokes = radicalStrokeCount.get(radical);
            if(strokes != null)
                stroke = strokes;
            List<String> rads = result.get(stroke);
            if(rads == null) {
                rads = new ArrayList<>();
                result.put(stroke, rads);
            }
            rads.add(radical);
        }
        return result;
    }
}
