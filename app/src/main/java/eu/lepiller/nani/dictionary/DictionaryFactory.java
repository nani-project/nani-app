package eu.lepiller.nani.dictionary;

import android.content.Context;
import android.os.Build;
import android.os.LocaleList;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;

import eu.lepiller.nani.result.ExampleResult;
import eu.lepiller.nani.result.KanjiResult;
import eu.lepiller.nani.result.Result;

public class DictionaryFactory {
    private static boolean initialized = false;

    private static ArrayList<Dictionary> dictionaries;
    private static File cacheDir, dataDir;
    private static File listFile;
    private static List<String> languages;

    private static final int LIST_PARSER_NONE = 0;
    private static final int LIST_PARSER_SYNOPSIS = 1;
    private static final int LIST_PARSER_DESCRIPTION = 2;
    private static final String TAG = "FACTORY";

    private DictionaryFactory() {}

    private static void initialize(Context context) {
        cacheDir = context.getCacheDir();
        dataDir = context.getFilesDir();
        listFile = new File(dataDir + "/list");
        languages = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList l = context.getResources().getConfiguration().getLocales();
            for(int i=0;i<l.size();i++) {
                Locale locale = l.get(i);
                languages.add(locale.getISO3Language());
                languages.add(locale.getLanguage());
                languages.add(locale.getLanguage().split("[_-]+]")[0]);
            }
        } else {
            Locale locale = context.getResources().getConfiguration().locale;
            languages.add(locale.getISO3Language());
            languages.add(locale.getLanguage());
            languages.add(locale.getLanguage().split("[_-]+]")[0]);
        }
        languages.add("en");
        Log.d(TAG, languages.toString());
        updatePackageList();

        initialized = true;
    }

    private static String chooseLanguage(Map<String, StringBuilder> data) {
        for(String l: languages) {
            if(data.containsKey(l)) {
                StringBuilder sb = data.get(l);
                if(sb != null)
                    return sb.toString();
            }
        }
        return null;
    }

    public static List<String> getLangs(Context context) {
        List<String> langs = new ArrayList<>();
        for(Dictionary d: getDictionaries(context)) {
            if(d.getLang().compareTo("") == 0)
                continue;
            if(langs.contains(d.getLang()))
                continue;
            langs.add(d.getLang());
        }
        Collections.sort(langs);
        return langs;
    }

    public static void updatePackageList() {
        dictionaries = new ArrayList<>();
        if(listFile.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(listFile));
                String line;

                Map<String, StringBuilder> synopsis = new HashMap<>();
                Map<String, StringBuilder> description = new HashMap<>();
                String sha256 = "";
                int size = 0;
                String type = "";
                int entries = 0;
                String url = "";
                String name = "";
                int current = LIST_PARSER_NONE;
                String currentLanguage = "";
                String lang = "";

                Dictionary d = null;
                while((line = br.readLine()) != null) {
                    if(line.isEmpty()) {
                        // create dictionary
                        if(type.compareTo("radk") == 0) {
                            d = new RadicalDict(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                            Log.d("FACTORY", "radk: " + (d.isDownloaded()? "downloaded": "not downloaded"));
                        } else if (type.compareTo("jmdict") == 0) {
                            d = new JMDict(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("jmnedict") == 0) {
                            d = new JMDict(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("wadoku") == 0) {
                            d = new WadokuResultDictionary(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("jibiki") == 0) {
                            d = new Jibiki(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("wadoku_pitch") == 0) {
                            d = new WadokuPitchDictionary(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("kanjidic") == 0) {
                            d = new KanjiDict(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("ksvg") == 0) {
                            d = new KanjiVG(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        } else if (type.compareTo("tatoeba") == 0) {
                            d = new TatoebaDictionary(name,
                                    chooseLanguage(synopsis),
                                    chooseLanguage(description),
                                    cacheDir, dataDir, url, size, entries, sha256, lang);
                        }

                        if(d != null) {
                            if (d.isDownloaded()) {
                                dictionaries.add(0, d);
                            } else {
                                dictionaries.add(d);
                            }
                        }

                        synopsis = new HashMap<>();
                        description = new HashMap<>();
                        sha256 = "";
                        size = 0;
                        type = "";
                        entries = 0;
                        url = "";
                        name = "";
                        currentLanguage = "";
                        lang = "";
                        d = null;
                    }

                    if(line.startsWith("[")) {
                        name = line.substring(1, line.length() - 1);
                    } else if(line.startsWith("    ")) {
                        StringBuilder sb;
                        switch (current) {
                            case LIST_PARSER_DESCRIPTION:
                                sb = description.get(currentLanguage);
                                if(sb != null) {
                                    sb.append(" ");
                                    sb.append(line.trim());
                                }
                                break;
                            case LIST_PARSER_SYNOPSIS:
                                sb = synopsis.get(currentLanguage);
                                if(sb != null) {
                                    sb.append(" ");
                                    sb.append(line.trim());
                                }
                                break;
                            default:
                                break;
                        }
                    } else if(line.startsWith("synopsis=")) {
                        String[] content = line.split("=");
                        currentLanguage = content[1];
                        current = LIST_PARSER_SYNOPSIS;
                        StringBuilder sb = new StringBuilder();
                        sb.append(line.substring(10 + currentLanguage.length()).trim());
                        synopsis.put(currentLanguage, sb);
                    } else if(line.startsWith("description=")) {
                        String[] content = line.split("=");
                        currentLanguage = content[1];
                        current = LIST_PARSER_DESCRIPTION;
                        StringBuilder sb = new StringBuilder();
                        sb.append(line.substring(13 + currentLanguage.length()).trim());
                        description.put(currentLanguage, sb);
                    } else if(line.startsWith("sha256=")) {
                        sha256 = line.substring(7);
                    } else if(line.startsWith("type=")) {
                        type = line.substring(5);
                    } else if(line.startsWith("url=")) {
                        url = line.substring(4);
                    } else if(line.startsWith("size=")) {
                        size = Integer.parseInt(line.substring(5));
                    } else if(line.startsWith("lang=")) {
                        lang = line.substring(5);
                    } else if(line.startsWith("entries=")) {
                        entries = Integer.parseInt(line.substring(8));
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void prepare(Context context) {
        if(!initialized)
            initialize(context);
    }

    public static Dictionary getByName(Context context, String name) {
        prepare(context);

        for(Dictionary d: dictionaries) {
            if (d.getName().equals(name))
                return d;
        }

        return null;
    }

    public static ArrayList<Result> search(String text) throws DictionaryException {
        if(!initialized)
            throw new NoDictionaryException();

        int available = 0;
        LinkedHashMap<String, Result> results = new LinkedHashMap<>();

        for(Dictionary d: dictionaries) {
            if (d instanceof ResultDictionary && d.isDownloaded()) {
                available++;
                List<Result> dr = ((ResultDictionary) d).search(text);
                if(dr != null) {
                    for(Result r: dr) {
                        try {
                            augment(r);
                        } catch (IncompatibleFormatException e) {
                            e.printStackTrace();
                        }
                        String k = r.getKanji();
                        if(results.containsKey(k)) {
                            Result res = results.get(k);
                            if(res != null)
                                res.merge(r);
                        } else {
                            results.put(k, r);
                        }
                    }
                }
            }
        }

        if(available == 0) {
            throw new NoDictionaryException();
        }

        return new ArrayList<>(results.values());
    }

    public static KanjiResult searchKanji(String kanji) throws DictionaryException {
        if(!initialized)
            throw new NoDictionaryException();

        Stack<KanjiResult> results = new Stack<>();

        for(Dictionary d: dictionaries) {
            if (d instanceof KanjiDictionary && d.isDownloaded()) {
                KanjiResult kanjiResult = ((KanjiDictionary) d).search(kanji);
                if(kanjiResult != null) {
                    results.add(kanjiResult);
                }
            }
        }

        Log.d(TAG, "search kanji, " + results.size() + " results");

        if(results.isEmpty())
            return null;

        KanjiResult res = results.pop();
        for(KanjiResult r: results) {
            res.merge(r);
        }

        return res;
    }

    public static List<ExampleResult> searchExamples(String word) throws DictionaryException {
        if(!initialized)
            throw new NoDictionaryException();

        List<ExampleResult> results = new ArrayList<>();
        for(Dictionary d: dictionaries) {
            if(d instanceof ExampleDictionary && d.isDownloaded()) {
                List<ExampleResult> r = ((ExampleDictionary) d).search(word);
                if(r != null)
                    results.addAll(r);
            }
        }

        Log.d(TAG, "search examples, " + results.size() + " results");

        return results;
    }

    private static void augment(Result r) throws IncompatibleFormatException {
        for(Dictionary d: dictionaries) {
            if(d instanceof ResultAugmenterDictionary && d.isDownloaded()) {
                Log.d(TAG, "Augmenting " + r.getKanji());
                ((ResultAugmenterDictionary) d).augment(r);
            }
        }
    }

    public static Dictionary get(int position, @Nullable String lang) {
        if(lang == null || lang.isEmpty())
            return dictionaries.get(position);

        int p = 0;
        for(Dictionary d: dictionaries) {
            if(d.getLang().isEmpty() || d.getLang().equals(lang)) {
                if(p == position)
                    return d;
                p++;
            }
        }
        return null;
    }

    public static ArrayList<Dictionary> getDictionaries(Context context) {
        if(!initialized)
            initialize(context);

        return dictionaries;
    }

    public static ArrayList<Dictionary> getDictionaries(Context context, String langFilter) {
        if(!initialized)
            initialize(context);

        if(langFilter.compareTo("") == 0)
            return dictionaries;

        ArrayList<Dictionary> dicos = new ArrayList<>();
        for(Dictionary d: dictionaries) {
            if(d.getLang().compareTo("") == 0 || d.getLang().compareTo(langFilter) == 0)
                dicos.add(d);
        }
        return dicos;
    }

    public static RadicalDict getRadicalDictionary(Context context) throws NoDictionaryException {
        if(!initialized)
            initialize(context);

        for(Dictionary d: dictionaries) {
            if(d.isDownloaded() && d instanceof RadicalDict) {
                return (RadicalDict) d;
            }
        }

        throw new NoDictionaryException();
    }

    public static URL getListUrl() throws MalformedURLException {
        return new URL("https://nani.lepiller.eu/dicos/list");
    }

    public static File getListFile() {
        return listFile;
    }
}
