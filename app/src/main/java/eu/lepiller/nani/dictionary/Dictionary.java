package eu.lepiller.nani.dictionary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.Pair;

import androidx.core.content.res.ResourcesCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import eu.lepiller.nani.R;

abstract public class Dictionary {
    private final String name;
    private final String description, fullDescription, lang;
    private final int expectedFileSize;
    private final int expectedEntries;
    private final String sha256;
    private final File file, temporaryFile;

    private Drawable drawable, newDrawable;

    Dictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, int fileSize, int entries, String hash, String lang) {
        this.name = name;
        this.description = description;
        this.fullDescription = fullDescription;
        expectedEntries = entries;
        expectedFileSize = fileSize;
        sha256 = hash;
        this.lang = lang;
        this.file = new File(dataDir, "/dico/" + name);
        this.temporaryFile = new File(cacheDir, "/dico/" + name);
    }

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public String getFullDescription() {
        return fullDescription;
    }
    public int getExpectedFileSize() {
        return expectedFileSize;
    }
    int getExpectedEntries() {
        return expectedEntries;
    }
    String getSha256() {
        return sha256;
    }
    String getLang() {
        return lang;
    }
    abstract public boolean canUpdate();

    File getFile() {
        return file;
    }
    File getTemporaryFile() {
        return temporaryFile;
    }

    abstract public int getSize();

    abstract public boolean isDownloaded();
    abstract public void switchToCacheFile();

    abstract public int size();

    public abstract Map<String, Pair<File, File>> getDownloads();

    public Drawable getDrawable(Context context) {
        if(drawable == null) {
            int drawableResId = getDrawableId();
            drawable = ResourcesCompat.getDrawable(context.getResources(), drawableResId, context.getTheme());
        }
        return drawable;
    }

    private static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private static Bitmap drawableToBitmap(Drawable drawable, int width, int height) {
        Bitmap background = drawableToBitmap(drawable);
        return Bitmap.createScaledBitmap(background, width, height, false);
    }

    public Drawable getNewDrawable(Context context) {
        if(newDrawable == null) {
            Bitmap big = Bitmap.createBitmap(640, 640, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(big);

            // Ensure we have the base icon
            getDrawable(context);

            Drawable star = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_star, context.getTheme());

            canvas.drawBitmap(drawableToBitmap(drawable, 640, 640), 0, 0, null);
            canvas.drawBitmap(drawableToBitmap(star, 220, 220), 390, 390, null);
            newDrawable = new BitmapDrawable(context.getResources(), big);
        }
        return newDrawable;
    }

    abstract int getDrawableId();

    abstract public void remove();

    public void removeTemporary() {
        if(getTemporaryFile().exists())
            if(!getTemporaryFile().delete())
                Log.w("DICTIONARY", getTemporaryFile() + " was not deleted as expected.");
    }

    public static String readSha256FromFile(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        char[] data = new char[4096];
        FileReader fr = new FileReader(file);
        int count;
        while((count = fr.read(data)) != -1) {
            sb.append(data, 0, count);
        }

        return sb.toString().trim();
    }

    static String sha256OfFile(File file) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if(digest == null)
            return null;

        digest.reset();
        try {
            byte[] data = new byte[4096];
            FileInputStream fr = new FileInputStream(file);
            int count;
            while((count = fr.read(data)) != -1) {
                digest.update(data, 0, count);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] result = digest.digest();
        return String.format("%0" + (result.length*2) + "X", new BigInteger(1, result)).toLowerCase();
    }
}
