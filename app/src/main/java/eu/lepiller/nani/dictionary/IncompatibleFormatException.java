package eu.lepiller.nani.dictionary;

public class IncompatibleFormatException extends DictionaryException {
    private final String name;

    IncompatibleFormatException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
