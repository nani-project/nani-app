package eu.lepiller.nani.dictionary;

import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class FileDictionary extends Dictionary {
    static final String encoding = "UTF-8";

    interface Huffman {
    }

    static class HuffmanTree implements Huffman {
        Huffman left, right;
        HuffmanTree(Huffman left, Huffman right) {
            this.left = left;
            this.right = right;
        }
    }
    static class HuffmanValue implements Huffman {
        String character;
        HuffmanValue(String character) {
            this.character = character;
        }
    }

    static abstract class Parser<T> {
        abstract T parse(RandomAccessFile file) throws IOException;
    }

    static abstract class TrieParser<T> {
        protected Parser<List<T>> valParser;
        TrieParser(Parser<List<T>> parser) {
            valParser = parser;
        }

        final List<T> decodeVals(RandomAccessFile file, long pos) throws IOException {
            seek(file, pos);
            return valParser.parse(file);
        }

        void seek(RandomAccessFile file, long pos) throws IOException {
            file.seek(pos);
        }

        abstract void skipVals(RandomAccessFile file, long pos) throws IOException;
    }

    static abstract class SingleTrieParser<T> extends TrieParser<T> {
        SingleTrieParser(Parser<T> parser) {
            super(new Parser<List<T>>() {
                @Override
                List<T> parse(RandomAccessFile file) throws IOException {
                    T obj = parser.parse(file);
                    ArrayList<T> list = new ArrayList<>();
                    list.add(obj);
                    return list;
                }
            });
        }
    }

    private final String mUrl;
    private final static String TAG = "FileDictionary";

    FileDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url,
                   int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, fileSize, entries, hash, lang);
        mUrl = url;
    }

    @Override
    public boolean isDownloaded() {
        Log.d("FILEDICT", getFile().getAbsolutePath());
        return getFile().exists();
    }

    @Override
    public void switchToCacheFile() {
        if(checkTemporaryFile()) {
            if(getFile().exists()) {
                if(!getFile().delete())
                    Log.w(TAG, getFile() + " was not deleted as expected.");
            }

            if(!getTemporaryFile().renameTo(getFile()))
                Log.w(TAG, getTemporaryFile() + " was not renamed to " + getFile() + " as expected.");

            File sha256 = new File(getFile() + ".sha256");
            File sha256Temp = new File(getTemporaryFile() + ".sha256");

            if(!sha256Temp.renameTo(sha256))
                Log.w(TAG, sha256Temp + " was not renamed to " + sha256 + " as expected.");
        }
    }

    private boolean checkTemporaryFile() {
        File f = getTemporaryFile();
        if(f.exists()) {
            String expected = getSha256();
            if (expected != null) {
                String hash = sha256OfFile(f);
                Log.v(TAG, "expected: " + expected + "; actual: " + hash);
                return expected.compareTo(hash) == 0;
            }
            return true;
        }
        return false;
    }

    public boolean canUpdate() {
        File sha256 = new File(getFile() + ".sha256");
        if(!sha256.exists())
            return false;

        try {
            String current = readSha256FromFile(sha256);
            String latest = getSha256();

            return latest.compareTo(current) != 0;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public Map<String, Pair<File, File>> getDownloads() {
        HashMap<String, Pair<File, File>> result = new HashMap<>();
        Pair<File, File> pair = new Pair<>(getTemporaryFile(), getFile());
        result.put(mUrl, pair);
        return result;
    }

    @Override
    public void remove() {
        if(getFile().exists())
            if(!getFile().delete())
                Log.w(TAG, getFile() + " was not deleted as expected.");
        if(getTemporaryFile().exists())
            if(!getTemporaryFile().delete())
                Log.w(TAG, getTemporaryFile() + " was not deleted as expected.");
    }


    public int getSize() {
        if(getFile().exists())
            return (int) (getFile().length());
        else if(getTemporaryFile().exists())
            return (int) (getTemporaryFile().length());
        else
            return 0;
    }

    static void logHuffman(Huffman h, ArrayList<Boolean> address) {
        if (h instanceof ResultDictionary.HuffmanValue) {
            Log.v(TAG, "HUFF: " + ((ResultDictionary.HuffmanValue) h).character + " -> " + address.toString());
        } else if(h instanceof ResultDictionary.HuffmanTree) {
            ArrayList<Boolean> address_l = new ArrayList<>(address);
            address_l.add(false);
            ArrayList<Boolean> address_r = new ArrayList<>(address);
            address_r.add(true);
            logHuffman(((JMDict.HuffmanTree) h).left, address_l);
            logHuffman(((JMDict.HuffmanTree) h).right, address_r);
        }
    }

    static class StringParser extends Parser<String> {
        @Override
        String parse(RandomAccessFile file) throws IOException {
            byte b;
            ArrayList<Byte> bs = new ArrayList<>();
            while((b = file.readByte()) != 0) {
                bs.add(b);
            }
            byte[] str = new byte[bs.size()];
            for(int j=0; j<bs.size(); j++) {
                str[j] = bs.get(j);
            }
            return new String(str, encoding);
        }
    }

    static class ListParser<T> extends Parser<List<T>> {
        Parser<T> parser;
        int lenSize;

        ListParser(Parser<T> parser) {
            this(2, parser);
        }
        ListParser(int lenSize, Parser<T> parser) {
            this.parser = parser;
            this.lenSize = lenSize;
        }

        @Override
        List<T> parse(RandomAccessFile file) throws IOException {
            List<T> results = new ArrayList<>();
            int number;
            switch(lenSize) {
                case 1:
                    number = file.readByte();
                    break;
                case 2:
                    number = file.readShort();
                    break;
                default:
                    number = file.readInt();
            }
            for(int i=0; i<number; i++) {
                Log.d("LISTPARSER", "at position " + file.getFilePointer());
                results.add(parser.parse(file));
            }
            return results;
        }
    }

    static class HuffmanParser extends Parser<Huffman> {

        @Override
        Huffman parse(RandomAccessFile file) throws IOException {
            byte b = file.readByte();
            if(b == 1) {
                Huffman left = new HuffmanParser().parse(file);
                Huffman right = new HuffmanParser().parse(file);

                return new HuffmanTree(left, right);
            } else if (b == 0) {
                Log.v(TAG, "Skipping byte " + file.readByte());
                return new HuffmanValue("");
            } else {
                ArrayList<Byte> bs = new ArrayList<>();
                bs.add(b);
                while((b = file.readByte()) != 0) {
                    bs.add(b);
                }
                byte[] array = new byte[bs.size()];
                for(int i=0; i<bs.size(); i++) {
                    array[i] = bs.get(i);
                }
                return new HuffmanValue(new String(array, encoding));
            }
        }
    }

    static class HuffmanStringParser extends Parser<String> {
        Huffman huffman;
        HuffmanStringParser(Huffman huffman) {
            this.huffman = huffman;
        }

        @Override
        String parse(RandomAccessFile file) throws IOException {
            StringBuilder b = new StringBuilder();
            ArrayList<Boolean> bits = new ArrayList<>();
            String c = null;
            ResultDictionary.Huffman h = huffman;
            while(c == null || !c.isEmpty()) {
                if(h instanceof ResultDictionary.HuffmanValue) {
                    c = ((ResultDictionary.HuffmanValue) h).character;
                    //Log.v(TAG, "Huffman read: " + c);
                    b.append(c);
                    h = huffman;
                } else if(h instanceof ResultDictionary.HuffmanTree) {
                    if(bits.isEmpty()) {
                        byte by = file.readByte();
                        //Log.v(TAG, "Read byte for huffman: " + by);
                        for(int i = 7; i>-1; i--) {
                            bits.add((by&(1<<i))!=0);
                        }
                        //Log.v(TAG, "Read byte for huffman: " + bits);
                    }

                    Boolean bo = bits.get(0);
                    bits.remove(0);
                    h = bo? ((ResultDictionary.HuffmanTree) h).right: ((ResultDictionary.HuffmanTree) h).left;
                }
            }

            return b.toString();
        }
    }

    static class TrieSearchParam {
        public final byte[] key;
        public final byte[] partialKey;
        public final long pos;

        public TrieSearchParam(long pos, byte[] key, byte[] partialKey){
            this.pos = pos;
            this.key = key;
            this.partialKey = partialKey;
        }
    }

    static<T> List<T> searchTrie(RandomAccessFile file, long pos, byte[] key, int limit, TrieParser<T> decoder) throws IOException {
        ArrayList<TrieSearchParam> queue = new ArrayList<>();
        ArrayList<T> results = new ArrayList<>();
        queue.add(new TrieSearchParam(pos, key, new byte[0]));

        byte[] partialKey;

        while(queue.size() > 0) {
            pos = queue.get(0).pos;
            key = queue.get(0).key;
            partialKey = queue.get(0).partialKey;
            queue.remove(0);

            if (key.length == 0) {
                results.addAll(decoder.decodeVals(file, pos));
                limit -= results.size();
                if (limit <= 0)
                    return results;
            }

            // if looking for '*', we actually look for '' or '?*'
            if(key.length > 0 && key[0] == '*' && partialKey.length == 0) {
                byte[] nkeyempty = new byte[key.length-1];
                System.arraycopy(key, 1, nkeyempty, 0, key.length-1);
                byte[] nkeyoption = new byte[key.length+1];
                nkeyoption[0] = '?';
                System.arraycopy(key, 0, nkeyoption, 1, key.length);

                queue.add(new TrieSearchParam(pos, nkeyempty, partialKey));
                queue.add(new TrieSearchParam(pos, nkeyoption, partialKey));
                continue;
            }

            file.seek(pos);
            decoder.skipVals(file, pos);
            int transitionCount = file.readByte();
            for (int i = 0; i < transitionCount; i++) {
                // go through each transitions. If a transition is selected, add it to the back of the queue
                byte letter = file.readByte();
                if (key.length == 0) {
                    long nextPos = file.readInt();
                    // if we already reached the end,
                    queue.add(new TrieSearchParam(nextPos, key, partialKey));
                } else if (key[0] == '?') {
                    byte first = partialKey.length > 0? partialKey[0]: letter;
                    int byteCount = ((first & 0b10000000) == 0)? 1: ((first & 0b11100000) == 0b11000000)? 2: ((first & 0b11110000) == 0b11100000)? 3: 4;
                    StringBuilder partial = new StringBuilder();
                    for (byte b : partialKey) {
                        partial.append(String.format("%02x", b));
                        partial.append(" ");
                    }
                    Log.d(TAG, "?: partialKey is " + partial.toString() + ", taking transition " + String.format("%02x", letter));
                    if(partialKey.length == byteCount-1) {
                        long nextPos = file.readInt();
                        byte[] nkey = new byte[key.length-1];
                        System.arraycopy(key, 1, nkey, 0, key.length-1);
                        queue.add(new TrieSearchParam(nextPos, nkey, new byte[0]));
                    } else {
                        long nextPos = file.readInt();
                        byte[] npartialKey = new byte[partialKey.length + 1];
                        System.arraycopy(partialKey, 0, npartialKey, 0, partialKey.length);
                        npartialKey[partialKey.length] = letter;
                        queue.add(new TrieSearchParam(nextPos, key, npartialKey));
                    }
                } else if (key[0] == letter) {
                    long nextPos = file.readInt();
                    Log.v(TAG, "Taking transition "+letter+" to " + nextPos);
                    byte[] nkey = new byte[key.length-1];
                    System.arraycopy(key, 1, nkey, 0, key.length-1);
                    queue.add(new TrieSearchParam(nextPos, nkey, partialKey));
                    // can only be one transition like this, so we can stop early
                    break;
                } else {
                    file.skipBytes(4);
                }
            }

            if(results.size() >= limit)
                break;
        }
        return results;
    }
}
