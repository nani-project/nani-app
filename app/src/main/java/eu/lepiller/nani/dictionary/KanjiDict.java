package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.KanjiResult;

public class KanjiDict extends KanjiDictionary {
    final private static String TAG = "KANJIDIC";
    private Huffman readingHuffman, meaningHuffman;

    KanjiDict(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    public void remove() {
        super.remove();
        readingHuffman = null;
        meaningHuffman = null;
    }

    @Override
    int getDrawableId() {
        return R.drawable.ic_nani_edrdg;
    }

    class ResultParser extends Parser<KanjiResult> {
        String kanji;
        ResultParser(String kanji) {
            this.kanji = kanji;
        }

        @Override
        KanjiResult parse(RandomAccessFile file) throws IOException {
            int stroke = file.readByte();
            Log.d(TAG, "strokes: " + stroke);

            List<String> senses = new ListParser<>(new HuffmanStringParser(meaningHuffman)).parse(file);
            List<KanjiResult.Sense> meanings = new ArrayList<>();
            for(String s: senses) {
                meanings.add(new KanjiResult.Sense(KanjiDict.this.getLang(), s));
            }
            List<String> kun = new ListParser<>(new HuffmanStringParser(readingHuffman)).parse(file);
            List<String> on = new ListParser<>(new HuffmanStringParser(readingHuffman)).parse(file);
            List<String> nanori = new ListParser<>(new HuffmanStringParser(readingHuffman)).parse(file);

            return new KanjiResult(kanji, stroke, meanings, kun, on, nanori, null, null);
        }
    }

    @Override
    KanjiResult search(final String kanji) throws IncompatibleFormatException {
        if (isDownloaded()) {
            try {
                Log.d(TAG, "search for kanji " + kanji);
                RandomAccessFile file = new RandomAccessFile(getFile(), "r");
                byte[] header = new byte[16];
                int l = file.read(header);
                if (l != header.length)
                    return null;

                // Check file format version
                if(!Arrays.equals(header, "NANI_KANJIDIC001".getBytes())) {
                    StringBuilder error = new StringBuilder("search: incompatible header: [");
                    boolean first = true;
                    for(byte b: header) {
                        if(first)
                            first = false;
                        else
                            error.append(", ");
                        error.append(b);
                    }
                    error.append("].");
                    Log.d(TAG, error.toString());
                    throw new IncompatibleFormatException(getName());
                }

                Log.d(TAG, "header OK");

                byte[] search = kanji.toLowerCase().getBytes();
                file.skipBytes(4); // size
                meaningHuffman = new HuffmanParser().parse(file);
                readingHuffman = new HuffmanParser().parse(file);
                long kanjiTriePos = file.getFilePointer();

                Log.d(TAG, "trie pos: " + kanjiTriePos);

                List<KanjiResult> results = searchTrie(file, kanjiTriePos, search, 1, new SingleTrieParser<KanjiResult>(new ResultParser(kanji)) {
                    @Override
                    public void skipVals(RandomAccessFile file1, long pos) throws IOException {
                        file1.seek(pos);
                        file1.skipBytes(4);
                    }

                    @Override
                    void seek(RandomAccessFile file, long pos) throws IOException {
                        file.seek(pos);
                        file.seek(file.readInt());
                    }
                });
                if(results.isEmpty())
                    return null;
                return results.get(0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
