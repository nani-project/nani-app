package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.Result;

class WadokuPitchDictionary extends ResultAugmenterDictionary {
    private static final String TAG = "PITCH";
    private Huffman huffman = null;
    private long triePos;

    WadokuPitchDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    int getDrawableId() {
        return R.drawable.wadoku;
    }

    private String findPitch(String kanji, String reading, RandomAccessFile file) throws IOException {
        String concat = kanji + reading;
        List<String> results = searchTrie(file, triePos, concat.getBytes(), 1, new SingleTrieParser<String>(new HuffmanStringParser(huffman)) {
            @Override
            public void skipVals(RandomAccessFile file, long pos) throws IOException {
                file.seek(pos);
                new HuffmanStringParser(huffman).parse(file);
            }
        });
        if(results.isEmpty())
            return null;
        return results.get(0);
    }

    @Override
    void augment(Result r) throws IncompatibleFormatException {
        try {
            RandomAccessFile file = new RandomAccessFile(getFile(), "r");

            if(huffman == null) {
                byte[] header = new byte[13];
                int l = file.read(header);
                if (l != header.length)
                    return;

                if (!Arrays.equals(header, "NANI_PITCH001".getBytes()))
                    throw new IncompatibleFormatException(getName());

                // number of entries
                file.readInt();

                huffman = new HuffmanParser().parse(file);
                logHuffman(huffman, new ArrayList<>());

                triePos = file.getFilePointer();
            }

            List<String> kanjis = r.getAlternatives();
            List<Result.Reading> readings = r.getReadings();

            for(String k: kanjis) {
                for(Result.Reading read: readings) {
                    List<String> readingKanjis = read.getKanjis();
                    if(readingKanjis != null && readingKanjis.size() > 0 && !readingKanjis.contains(k)) {
                        // this is not a reading for the current kanji, so continue to the next
                        // reading
                        continue;
                    }
                    List<String> innerReadings = read.getReadings();
                    for(String reading: innerReadings) {
                        Log.d(TAG, "Searching pitch for " + k + reading);
                        String pitch = findPitch(k, reading, file);
                        if(pitch != null) {
                            Log.d(TAG, "Found " + pitch);
                            read.addPitch(pitch);
                        }
                    }
                }
            }

            if(kanjis.size() == 0) {
                for(Result.Reading read: readings) {
                    List<String> innerReadings = read.getReadings();
                    for(String reading: innerReadings) {
                        String pitch = findPitch("", reading, file);
                        if(pitch != null) {
                            Log.d(TAG, "Found " + pitch);
                            read.addPitch(pitch);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
