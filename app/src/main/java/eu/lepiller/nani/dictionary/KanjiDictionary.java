package eu.lepiller.nani.dictionary;

import java.io.File;

import eu.lepiller.nani.result.KanjiResult;

public abstract class KanjiDictionary extends FileDictionary {
    KanjiDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    abstract KanjiResult search(final String kanji) throws IncompatibleFormatException;
}
