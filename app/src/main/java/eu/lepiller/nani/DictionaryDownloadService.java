package eu.lepiller.nani;

import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;

import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.lepiller.nani.dictionary.Dictionary;
import eu.lepiller.nani.dictionary.DictionaryFactory;

public class DictionaryDownloadService extends JobService {
    private NotificationCompat.Builder builder;
    private JobParameters parameters;

    static public class DownloadData {
        public String currentName;
        public int currentProgress;
        public ArrayList<String> downloading;

        DownloadData(String name, int progress, ArrayList<String> next) {
            currentName = name;
            currentProgress = progress;
            downloading = next;
        }
    }

    private static MutableLiveData<DownloadData> data;

    private static final String TAG = "DicoDownloadService";

    public static LiveData<DownloadData> getData() {
        if(data == null)
            data = new MutableLiveData<>();

        return data;
    }

    private Downloader downloadThread;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        parameters = jobParameters;
        String name = jobParameters.getExtras().getString(DictionaryDownloadActivity.EXTRA_DICTIONARY);

        builder =  new NotificationCompat.Builder(this, App.DICTIONARY_DOWNLOAD_NOTIFICATION_CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentText(getString(R.string.downloading))
                .setOnlyAlertOnce(true);
        downloadThread = new Downloader(name, () -> {
            jobFinished(jobParameters, false);
        });
        new Thread(downloadThread).start();

        updateNotification(-1, name);

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        updateNotification(0, null);
        downloadThread.stop();
        return true;
    }


    void updateNotification(int progress, String name) {
        PendingIntent pendingIntent;
        if(name == null) {
            Intent notificationIntent = new Intent(this, DictionaryActivity.class);
            pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)? PendingIntent.FLAG_IMMUTABLE: 0);
            builder.setContentTitle(getString(R.string.downloading));
        } else {
            Intent notificationIntent = new Intent(this, DictionaryDownloadActivity.class);
            notificationIntent.putExtra(DictionaryDownloadActivity.EXTRA_DICTIONARY, name);
            pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)? PendingIntent.FLAG_IMMUTABLE: 0);
            builder.setContentTitle(name);
        }

        builder.setContentIntent(pendingIntent)
               .setProgress(100, progress, (progress < 0));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            setNotification(parameters, 1, builder.build(),
                    JobService.JOB_END_NOTIFICATION_POLICY_DETACH);
        } else {
            startForeground(1, builder.build());
        }

        final DownloadData downloadData = new DownloadData(name, progress, new ArrayList<>());

        Handler threadHandler = new Handler(Looper.getMainLooper());
        threadHandler.post(() -> {
            if(data == null)
                data = new MutableLiveData<>();
            data.setValue(downloadData);
        });
    }

    private class Downloader implements Runnable {
        private final Runnable onStop;
        private final String name;
        private boolean wantsStop;

        Downloader(String name, Runnable onStop) {
            this.name = name;
            this.onStop = onStop;
            this.wantsStop = false;
        }

        @Override
        public void run() {
            updateNotification(-1, null);
            doDownload(name);
            updateNotification(-1, null);
            Handler threadHandler = new Handler(Looper.getMainLooper());
            threadHandler.post(onStop);
        }

        private void publishProgress(int progress, String name) {
            updateNotification(progress, name);
        }

        private void doDownload(String name) {
            Dictionary d = DictionaryFactory.getByName(DictionaryDownloadService.this, name);
            if(d == null)
                return;

            for (Map.Entry<String, Pair<File, File>> e : d.getDownloads().entrySet()) {
                try {
                    String uri = e.getKey();
                    File temporaryFile = e.getValue().first;
                    File cacheFile = e.getValue().second;
                    createParent(cacheFile);

                    boolean newFile = downloadSha256(new URL(uri + ".sha256"), new File(temporaryFile + ".sha256"));
                    if(newFile) {
                        d.removeTemporary();
                    }

                    long expectedFileLength = getRange(new URL(uri));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                        updateEstimatedNetworkBytes(parameters, expectedFileLength, 0);
                    }
                    downloadFile(new URL(uri), temporaryFile, expectedFileLength, name);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            d.switchToCacheFile();
        }

        private boolean downloadSha256(URL url, File dest) throws IOException {
            createParent(dest);
            byte[] data = new byte[4096];
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                return true;

            InputStream input = connection.getInputStream();
            File old_file = new File(dest + ".old");
            deleteIfExists(old_file);
            if(dest.exists())
                rename(dest, old_file);

            FileOutputStream output = new FileOutputStream(dest);

            int count;
            while((count = input.read(data)) != -1) {
                if (isCancelled()) {
                    input.close();
                    return false;
                }
                output.write(data, 0, count);
            }

            if(old_file.exists()) {
                // Check that we continue to download the same file
                String old_hash = Dictionary.readSha256FromFile(old_file);
                String new_hash = Dictionary.readSha256FromFile(dest);
                return old_hash.compareTo(new_hash) != 0;
            }
            return true;
        }

        private long getRange(URL url) throws IOException {
            long expectedLength;
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            connection.connect();
            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                return -1;

            boolean acceptRanges = connection.getHeaderFields().containsKey("accept-ranges");
            expectedLength = connection.getContentLength();
            List<String> headers = connection.getHeaderFields().get("accept-ranges");
            if(headers != null) {
                for (String h : headers) {
                    if (h.toLowerCase().compareTo("none") == 0)
                        acceptRanges = false;
                }
            }

            if(acceptRanges)
                return expectedLength;
            return -1;
        }

        private void downloadFile(URL url, File dest, long expectedLength, String name) throws IOException {
            createParent(dest);
            long total = 0;
            byte[] data = new byte[4096];
            FileOutputStream output;

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if(expectedLength > 0 && dest.length() < expectedLength) {
                connection.addRequestProperty("Range", "bytes=" + dest.length() + "-" + (expectedLength-1));
                total = dest.length();
                output = new FileOutputStream(dest, true);
            } else {
                output = new FileOutputStream(dest);
            }
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK &&
                    connection.getResponseCode() != HttpURLConnection.HTTP_PARTIAL) {
                Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());
                return;
            }

            int fileLength = connection.getContentLength();
            InputStream input = connection.getInputStream();

            int count;
            int lastNotifiedProgress = 0;
            publishProgress(0, name);
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return;
                }
                total += count;
                output.write(data, 0, count);
                // publishing the progress....
                if (fileLength > 0) {// only if total length is known
                    float progress = (int) (total * 100 / fileLength);
                    if(lastNotifiedProgress < progress - 2) {
                        lastNotifiedProgress = (int)progress;
                        publishProgress((int) progress, name);
                        output.flush();
                    }
                }
            }
        }

        private boolean isCancelled() {
            return wantsStop;
        }

        private void createParent(File file) {
            if(file.getParentFile() == null || (!file.getParentFile().exists() && !file.getParentFile().mkdirs()))
                Log.w(TAG, "could not create parent of " + file);
        }

        private void deleteIfExists(File file) {
            if(file.exists()) {
                if(!file.delete())
                    Log.w(TAG, "could not delete file " + file);
            }
        }

        private void rename(File file, File dest) {
            if(file.exists()) {
                if(!file.renameTo(dest))
                    Log.w(TAG, "could not rename "+ file + " to " + dest);
            }
        }

        void stop() {
            this.wantsStop = true;
        }
    }
}
