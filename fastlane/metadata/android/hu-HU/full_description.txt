A Nani egy olyan alkalmazás, amely lehetővé teszi a teljes kétnyelvű japán szótár offline böngészését. Néhány jellemzője a következőket tartalmazza:

- Teljesen ingyenes, hirdetés vagy követés nélkül
- Keresse a szót kandzsi, kiejtés vagy jelentés alapján
- Több szótár különböző funkciókat tesz lehetővé

Az első indításkor nem töltődik be szótár az alkalmazásba. Rajtad múlik, hogy melyik szótárat szeretnéd használni.
