Nani — програма, за допомогою якої ви зможете переглядати повністю двомовний словник японської мови в автономному режимі, без інтернету. Серед можливостей програми такі:

- Програма є повністю вільною, без реклами і стеження за користувачем
- Можна шукати слово за кандзі, вимовою або значенням
- Декілька словників із різними функціональними можливостями

Під час першого запуску програма не завантажує жодного словника. Ви самі можете вибрати словник, яким ви хочете скористатися.
